/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_LOGGER_INSTANCE_HPP_
#define _RGLIB_LOGGER_INSTANCE_HPP_

#include <memory>

#include "ILogger.hpp"

namespace rglib {

class LoggerInstance
{
public:
  ~LoggerInstance();

  static LoggerInstance& getInstance();

  int defaultLogger();
  int consoleLogger();
  int nullLogger();
  int log4cplus(const std::string& config);
  void setMaxLevel(ILogger::LogLevel level);
  ILogger& operator*();

  LoggerInstance(const LoggerInstance& rhs) = delete;
  LoggerInstance& operator=(const LoggerInstance& rhs) = delete;

private:
  std::unique_ptr<ILogger> m_logger;

  /* Singletone */
  LoggerInstance();
};

}       // namespace
#endif  // _RGLIB_LOGGER_INSTANCE_HPP_
