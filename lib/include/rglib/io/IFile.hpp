/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_I_FILE_HPP_
#define _RGLIB_I_FILE_HPP_

#include <memory>
#include <string>

#include "IIoStream.hpp"

namespace rglib {

enum OpenFlag {
  READ_ONLY   = 0x01,
  WRITE_ONLY  = 0x02,
  READ_WRITE  = 0x04,
  CREATE      = 0x08,
  TRUNC       = 0x10,
  APPEND      = 0x20,
};

class IFile:
    public IIoStream
{
public:
  static void create(std::unique_ptr<IFile>& file);

  IFile();
  virtual ~IFile();

  virtual int open(const std::string& path, int flags) = 0;
};

}       // namespace
#endif  // _RGLIB_I_FILE_HPP_
