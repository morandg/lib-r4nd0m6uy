/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_VECTOR_2D_HPP_
#define _RGLIB_VECTOR_2D_HPP_

namespace rglib {

class Vector2D
{
public:
  Vector2D();
  Vector2D(int x, int y);
  ~Vector2D();

  int getX() const;
  int getY() const;

private:
  int m_x;
  int m_y;
};

}       // namespace
#endif  // _RGLIB_I_EVENT_LOOP_HPP_
