/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_RECTANGLE_HPP_
#define _RGLIB_RECTANGLE_HPP_

#include "rglib/geometry/Vector2D.hpp"

namespace rglib {

class Rectangle
{
public:
  Rectangle();
  Rectangle(int x, int y, int width, int height);
  ~Rectangle();

  int getX() const;
  int getY() const;
  int getWidth() const;
  int getHeight() const;
  bool isIn(const Vector2D& point) const;

private:
  Vector2D m_position;  // Top left corner
  Vector2D m_size;
};

}       // namespace
#endif  // _RGLIB_RECTANGLE_HPP_
