/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_I_SOCKET_SERVER_HPP_
#define _RGLIB_I_SOCKET_SERVER_HPP_

#include <memory>
#include <string>

#include "ISocketClient.hpp"
#include "../io/IIoStream.hpp"

namespace rglib {

class ISocketServer
{
public:
  ISocketServer();
  virtual ~ISocketServer();

  static void create(std::unique_ptr<ISocketServer>& socket);

  virtual int bindIpV4(
      SocketType type,
      const std::string& interface,
      unsigned int port,
      int backlog) = 0;
  virtual void close() = 0;
  virtual int accept(std::unique_ptr<ISocketClient>& client) = 0;
  virtual FileDescriptor getFileDescriptor() = 0;
};

}       // namespace
#endif  // _RGLIB_I_SOCKET_SERVER_HPP_
