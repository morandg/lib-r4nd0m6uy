/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_I_SOCKET_CLIENT_HPP_
#define _RGLIB_I_SOCKET_CLIENT_HPP_

#include <memory>

#include "../io/IIoStream.hpp"
#include "SocketDefs.hpp"

namespace rglib {

class ISocketClient:
    public IIoStream
{
public:
  ISocketClient();
  virtual ~ISocketClient();

  // XXX
//  virtual int connect(
//      SocketDomain domain,
//      SocketType type,
//      const std::string& address) = 0;
};

}       // namespace
#endif  // _RGLIB_I_SOCKET_CLIENT_HPP_
