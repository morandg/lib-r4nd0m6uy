/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_I_EVENT_LOOP_HPP_
#define _RGLIB_I_EVENT_LOOP_HPP_

#include <memory>

#include "IHandledSignal.hpp"
#include "IHandledIo.hpp"
#include "ITimer.hpp"
#include "ITimedOut.hpp"

namespace rglib {

class IEventLoop
{
public:
  enum EventType
  {
    READ    = 0x01,
    PERSIST = 0x02
  };

  static void create(std::unique_ptr<IEventLoop>& eventLoop);

  IEventLoop();
  virtual ~IEventLoop();

  virtual int init() = 0;
  virtual int reinit() = 0;
  virtual int run() = 0;
  virtual int breakLoop() = 0;
  virtual int createTimer(
      std::unique_ptr<ITimer>& timer,
      const std::string& name,
      ITimedOut& timedOut) = 0;
  virtual int addHandledSignal(
      IHandledSignal& handler, SignalHandle signal) = 0;
  virtual int addHandledIo(IHandledIo& handler, int what) = 0;
  virtual void removeHandledIo(IHandledIo& handler) = 0;
};

}       // namespace
#endif  // _RGLIB_I_EVENT_LOOP_HPP_
