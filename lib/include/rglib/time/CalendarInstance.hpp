/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_CALENDAR_INSTANCE_HPP_
#define _RGLIB_CALENDAR_INSTANCE_HPP_

#include "ICalendar.hpp"

namespace rglib {

class CalendarInstance
{
public:
  static CalendarInstance& getInstance();

  ICalendar& get();
  void set(std::unique_ptr<ICalendar>& calendar);

  CalendarInstance(CalendarInstance const&) = delete;
  CalendarInstance& operator=(CalendarInstance const&) = delete;

private:
  std::unique_ptr<ICalendar> m_calendar;

  CalendarInstance();
  ~CalendarInstance();
};

#define RGLIB_GET_UTC_TIME(x) rglib::CalendarInstance::getInstance().get().getUtcTime(x)
#define RGLIB_GET_MONOTONIC_TIME(x) rglib::CalendarInstance::getInstance().get().getMonotonicTime(x)

}       // namespace
#endif  // _RGLIB_CALENDAR_INSTANCE_HPP_
