/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TIME_DURATION_HPP_
#define _RGLIB_TIME_DURATION_HPP_

#include <cstdint>
#include <string>

namespace rglib {

class TimeDuration
{
public:
  static const uint64_t NANO_PER_MS;
  static const uint64_t NANO_PER_SECOND;
  static const uint64_t SECONDS_PER_MINUTE;
  static const uint64_t SECONDS_PER_HOUR;
  static const uint64_t SECONDS_PER_DAY;

  static const TimeDuration& ZERO();
  static const TimeDuration& ONE_MILLISECOND();
  static const TimeDuration& ONE_SECOND();
  static const TimeDuration& ONE_MINUTE();
  static const TimeDuration& ONE_HOUR();
  static const TimeDuration& ONE_DAY();

  TimeDuration();
  TimeDuration(uint64_t sec, uint64_t nsec, bool isPositive = true);
  ~TimeDuration();

  uint64_t getSeconds() const;
  uint64_t getNseconds() const;
  bool isPositive() const;
  std::string toString() const;
  double toDouble() const;

  // CPP operators
  bool operator==(const TimeDuration& rhs) const;
  bool operator!=(const TimeDuration& rhs) const;
  bool operator<(const TimeDuration& rhs) const;
  bool operator<=(const TimeDuration& rhs) const;
  bool operator>(const TimeDuration& rhs) const;
  bool operator>=(const TimeDuration& rhs) const;
  TimeDuration operator+(const TimeDuration& rhs) const;
  TimeDuration operator+=(const TimeDuration& rhs);
  TimeDuration operator-(const TimeDuration& rhs) const;
  TimeDuration operator-=(const TimeDuration& rhs);
  TimeDuration operator*(const int& rhs) const;
  TimeDuration operator*=(const int& rhs);

private:
  uint64_t m_sec;
  uint64_t m_nsec;
  bool m_isPositive;
};

std::ostream& operator<<(std::ostream& os, const TimeDuration& duration);

}       // namespace
#endif  // _RGLIB_TIME_DURATION_HPP_
