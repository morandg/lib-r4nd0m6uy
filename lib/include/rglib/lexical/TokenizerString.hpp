/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TOKENIZER_HPP_
#define _RGLIB_TOKENIZER_HPP_

#include "rglib/lexical/ITokenizer.hpp"

namespace rglib {

class TokenizerString:
    public ITokenizer
{
public:
  TokenizerString();
  virtual ~TokenizerString();

  void setBuffer(const std::string& buffer) override;
  bool hasToken() const  override;
  bool nextToken(char& token) override;

private:
  unsigned int m_buffPos;
  std::string m_buffer;
};

}       // namespace
#endif  // _RGLIB_TOKENIZER_HPP_
