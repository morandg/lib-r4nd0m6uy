/**
 * Charguy chess: A DIY chess hardware
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TimerLibevent.hpp"

namespace rglib {

//--------------------------------------------------------------------------------------------
TimerLibevent::TimerLibevent(
    std::shared_ptr<EventBaseLibevent>& eventbase,
    const std::string& name,
    ITimedOut& timedOut):
  HandledByLibevent(eventbase),
  m_name(name),
  m_timedOut(timedOut),
  m_timeout(0)
{
}

//--------------------------------------------------------------------------------------------
TimerLibevent::~TimerLibevent()
{
}

//--------------------------------------------------------------------------------------------
void TimerLibevent::timedOut()
{
  m_timedOut.onTimeout(m_name);
}

//--------------------------------------------------------------------------------------------
const std::string& TimerLibevent::getName() const
{
  return m_name;
}

//--------------------------------------------------------------------------------------------
void TimerLibevent::start()
{
  struct timeval tvTimeout;

  tvTimeout.tv_sec = m_timeout / 1000;
  tvTimeout.tv_usec = (m_timeout % 1000) * 1000;

  event_add(getEvent(), &tvTimeout);
}

//--------------------------------------------------------------------------------------------
void TimerLibevent::restart()
{
  stop();
  start();
}

//--------------------------------------------------------------------------------------------
bool TimerLibevent::isRunning()
{
  return evtimer_pending(getEvent(), NULL) != 0;
}

//--------------------------------------------------------------------------------------------
void TimerLibevent::stop()
{
  event_del(getEvent());
}

//--------------------------------------------------------------------------------------------
void TimerLibevent::setTimeout(unsigned int ms)
{
  m_timeout = ms;
}

}       // namespace
