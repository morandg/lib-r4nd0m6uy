/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_EVENT_LOOP_LIBEVENT_HPP_
#define _RGLIB_EVENT_LOOP_LIBEVENT_HPP_

#include <unordered_map>

#include "rglib/event-loop/IEventLoop.hpp"

#include "HandledSignalLibevent.hpp"
#include "HandledIoLibevent.hpp"
#include "EventBaseLibevent.hpp"

namespace rglib {

class EventLoopLibevent:
    public IEventLoop
{
public:
  virtual ~EventLoopLibevent();

  // IEventLoop
  virtual int init() override;
  virtual int reinit() override;
  virtual int run() override;
  virtual int breakLoop() override;
  virtual int createTimer(
      std::unique_ptr<ITimer>& timer,
      const std::string& name,
      ITimedOut& timedOut) override;
  virtual int addHandledSignal(IHandledSignal& handler,
      SignalHandle signal) override;
  virtual int addHandledIo(IHandledIo& handler, int what) override;
  virtual void removeHandledIo(IHandledIo& handler) override;

  EventLoopLibevent(const EventLoopLibevent& rhs) = delete;
  EventLoopLibevent& operator=(const EventLoopLibevent& rhs) = delete;

private:
  std::shared_ptr<EventBaseLibevent> m_eventBase;
  std::unordered_map<SignalHandle, std::unique_ptr<HandledSignalLibevent>> m_handleSignals;
  std::unordered_map<FileDescriptor, std::unique_ptr<HandledIoLibevent>> m_handledIos;

  EventLoopLibevent();

  friend class IEventLoop;
};

}       // namespace
#endif  // _RGLIB_I_EVENT_LOOP_HPP_

