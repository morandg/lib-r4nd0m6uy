/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TIMER_LIBEVENT_HPP_
#define _RGLIB_TIMER_LIBEVENT_HPP_

#include "rglib/event-loop/ITimer.hpp"
#include "rglib/event-loop/ITimedOut.hpp"
#include "HandledByLibevent.hpp"
#include "EventBaseLibevent.hpp"

namespace rglib {

class TimerLibevent:
    public HandledByLibevent,
    public ITimer
{
public:
  TimerLibevent(
      std::shared_ptr<EventBaseLibevent>& eventbase,
      const std::string& name,
      ITimedOut& timedOut);
  virtual ~TimerLibevent();

  void timedOut();

  // ITimer
  virtual const std::string& getName() const override;
  virtual void start() override;
  virtual void stop() override;
  virtual void restart() override;
  virtual bool isRunning() override;
  virtual void setTimeout(unsigned int ms) override;

private:
  std::string m_name;
  ITimedOut& m_timedOut;
  int m_timeout;
};

}       // namespace
#endif  // _RGLIB_TIMER_LIBEVENT_HPP_
