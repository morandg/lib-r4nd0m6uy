/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>
#include <cerrno>

#include "EventLoopLibevent.hpp"
#include "TimerLibevent.hpp"

namespace rglib {

extern "C" {

//------------------------------------------------------------------------------
void libeventSignalCallback(evutil_socket_t fd, short what, void* data)
{
  IHandledSignal* handle = static_cast<IHandledSignal*>(data);

  assert(what & EV_SIGNAL);
  (void)what;

  handle->onSignal(fd);
}

//------------------------------------------------------------------------------
void libeventIoCallback(evutil_socket_t fd, short what, void* data)
{
  IHandledIo* handle = static_cast<IHandledIo*>(data);

  (void)fd;

  if((what & EV_READ) != 0)
    handle->onReadReady();
}
//------------------------------------------------------------------------------

void libeventTimerCallback(evutil_socket_t fd, short what, void* data)
{
  TimerLibevent* timer = static_cast<TimerLibevent*>(data);

  (void)fd;
  (void)what;

  assert(what & EV_TIMEOUT);

  timer->timedOut();
}

}   // extern "C"

//------------------------------------------------------------------------------
EventLoopLibevent::EventLoopLibevent():
    m_eventBase(new EventBaseLibevent())
{
}

//------------------------------------------------------------------------------
EventLoopLibevent::~EventLoopLibevent()
{
}

//------------------------------------------------------------------------------
int EventLoopLibevent::init()
{
  if(m_eventBase->getEventBase() == nullptr)
    return -ENOMEM;

  return 0;
}

//------------------------------------------------------------------------------
int EventLoopLibevent::reinit()
{
  return event_reinit(m_eventBase->getEventBase());
}

//------------------------------------------------------------------------------
int EventLoopLibevent::run()
{
  if(m_eventBase == nullptr)
    return -EFAULT;

  return event_base_loop(m_eventBase->getEventBase(), 0);
}

//------------------------------------------------------------------------------
int EventLoopLibevent::breakLoop()
{
  return event_base_loopbreak(m_eventBase->getEventBase());
}

//------------------------------------------------------------------------------
int EventLoopLibevent::createTimer(
    std::unique_ptr<ITimer>& timer,
    const std::string& name,
    ITimedOut& timedOut)
{
  struct event* ev;
  std::unique_ptr<TimerLibevent> newTimer(
      new TimerLibevent(m_eventBase, name, timedOut));

  ev = evtimer_new(
      m_eventBase->getEventBase(), libeventTimerCallback, newTimer.get());
  if(ev == nullptr)
    return -ENOMEM;

  newTimer->setEvent(ev);
  timer = std::move(newTimer);

  return 0;
}

//------------------------------------------------------------------------------
int EventLoopLibevent::addHandledSignal(
    IHandledSignal& handler, SignalHandle signal)
{
  struct event* ev;
  std::unique_ptr<HandledSignalLibevent> wrapped;

  if(m_eventBase == nullptr)
    return -EFAULT;

  wrapped.reset(new HandledSignalLibevent(m_eventBase, handler));
  ev = evsignal_new(
      m_eventBase->getEventBase(),
      signal,
      libeventSignalCallback,
      &handler);
  if(ev == nullptr)
    return -ENOMEM;

  if(event_add(ev, NULL))
  {
    event_free(ev);
    return -ENOMEM;
  }

  wrapped->setEvent(ev);
  m_handleSignals.emplace(signal, std::move(wrapped));

  return 0;
}

//------------------------------------------------------------------------------
int EventLoopLibevent::addHandledIo(IHandledIo& handler, int what)
{
  int libeventWhat = 0;
  struct event* ev;
  FileDescriptor handle;
  std::unique_ptr<HandledIoLibevent> wrapped;

  if(m_eventBase == nullptr)
    return -EFAULT;

  if((what & READ) != 0)
    libeventWhat |= EV_READ;
  if((what & PERSIST) != 0)
    libeventWhat |= EV_PERSIST;

  handle = handler.getFileDescriptor();
  wrapped.reset(new HandledIoLibevent(m_eventBase, handler));
  ev = event_new(
      m_eventBase->getEventBase(),
      handle,
      libeventWhat,
      libeventIoCallback,
      &handler);
  if(ev == nullptr)
    return -1;

  if(event_add(ev, nullptr))
    return -1;

  wrapped->setEvent(ev);
  m_handledIos.emplace(handle, std::move(wrapped));

  return 0;
}

//------------------------------------------------------------------------------
void EventLoopLibevent::removeHandledIo(IHandledIo& handler)
{
  auto foundHandler = m_handledIos.find(handler.getFileDescriptor());

  if(foundHandler == m_handledIos.end())
    return;

  event_del(foundHandler->second->getEvent());
  m_handledIos.erase(foundHandler);
}

}       // namespace

