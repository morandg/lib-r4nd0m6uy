/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include <unistd.h>

#include "SocketClientPosix.hpp"
#include "SocketServerPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void ISocketServer::create(std::unique_ptr<ISocketServer>& socket)
{
  socket.reset(new SocketServerPosix());
}

//------------------------------------------------------------------------------
SocketServerPosix::SocketServerPosix():
    m_fd(IIoStream::FD_INVALID),
    m_domain(SocketDomain::UNKOWN)
{
}

//------------------------------------------------------------------------------
SocketServerPosix::~SocketServerPosix()
{
  close();
}

//------------------------------------------------------------------------------
int SocketServerPosix::bindIpV4(
      SocketType type,
      const std::string& address,
      unsigned int port,
      int backlog)
{
  int reuseAddr = 1;
  int posixType;
  struct sockaddr_in sockInfo;
  int ret;

  close();

  if(type == SocketType::STREAM)
    posixType = SOCK_STREAM;
  else
    return -EINVAL;

  m_fd = socket(AF_INET, posixType, 0);
  if(m_fd < 0)
  {
    m_fd = IIoStream::FD_INVALID;
    return -errno;
  }

  if(address == "")
    sockInfo.sin_addr.s_addr = INADDR_ANY;
  else {
    ret = inet_pton(AF_INET, address.c_str(), &sockInfo.sin_addr.s_addr);

    if(ret < 0)
    {
      ret = -errno;
      close();
      return ret;
    }
    else if(ret == 0)
      return -EINVAL;
  }
  if(port > 0xFFFF)
    return -EINVAL;
  sockInfo.sin_port = htons(port);
  sockInfo.sin_family = AF_INET;

  // Don't fail if reuse address fails, let's see what happens in "bind"
  setsockopt(m_fd, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(int));

  if(bind(m_fd, (struct sockaddr*)&sockInfo, sizeof(sockInfo)))
  {
    ret = -errno;
    close();
    return ret;
  }

  if(listen(m_fd, backlog))
  {
    ret = -errno;
    close();
    return ret;
  }

  m_domain = SocketDomain::IPv4;

  return 0;
}

//------------------------------------------------------------------------------
void SocketServerPosix::close()
{
  if(m_fd != IIoStream::FD_INVALID)
  {
    ::close(m_fd);
    m_fd = IIoStream::FD_INVALID;
    m_domain = SocketDomain::UNKOWN;
  }
}

//------------------------------------------------------------------------------
int SocketServerPosix::accept(std::unique_ptr<ISocketClient>& client)
{
  if(m_domain == SocketDomain::IPv4)
    return acceptIpV4(client);
  else
    return -ENOTSUP;
}

//------------------------------------------------------------------------------
FileDescriptor SocketServerPosix::getFileDescriptor()
{
  return m_fd;
}

//------------------------------------------------------------------------------
int SocketServerPosix::acceptIpV4(std::unique_ptr<ISocketClient>& client)
{
  struct sockaddr_in remoteAddr;
  socklen_t remotAddSize = sizeof(remoteAddr);
  FileDescriptor clientFd;

  clientFd = ::accept(m_fd, (struct sockaddr*)&remoteAddr, &remotAddSize);
  if(clientFd < 0)
    return -errno;

  client.reset(new SocketClientPosix(clientFd));

  return 0;

}
}       // namespace
