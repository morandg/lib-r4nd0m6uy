/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include <cerrno>

#include "PipePosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void IPipe::create(std::unique_ptr<IPipe>& pipe)
{
  pipe.reset(new PipePosix());
}

//------------------------------------------------------------------------------
PipePosix::PipePosix()
{
}

//------------------------------------------------------------------------------
PipePosix::~PipePosix()
{
  close();
}

//------------------------------------------------------------------------------
int PipePosix::open()
{
  int pipeFds[2];

  if(m_readEnd.isOpen() && m_writeEnd.isOpen())
    return 0;

  if(pipe(pipeFds))
    return -errno;

  m_readEnd.setFileDescriptor(pipeFds[0]);
  m_writeEnd.setFileDescriptor(pipeFds[1]);

  return 0;
}

//------------------------------------------------------------------------------
void PipePosix::close()
{
  m_readEnd.close();
  m_writeEnd.close();
}

//------------------------------------------------------------------------------
IIoStream& PipePosix::getWriteEnd()
{
  return m_writeEnd;
}

//------------------------------------------------------------------------------
IIoStream& PipePosix::getReadEnd()
{
  return m_readEnd;
}

}       // namespace
