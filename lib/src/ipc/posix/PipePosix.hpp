/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_PIPE_POSIX_HPP_
#define _RGLIB_PIPE_POSIX_HPP_

#include "rglib/ipc/IPipe.hpp"
#include "../../io/posix/IoStreamPosix.hpp"

namespace rglib {

class PipePosix:
    public IPipe
{
public:
  virtual ~PipePosix();

  virtual int open() override;
  virtual void close() override;
  virtual IIoStream& getWriteEnd() override;
  virtual IIoStream& getReadEnd() override;

  PipePosix(const PipePosix& rhs) = delete;
  PipePosix& operator=(const PipePosix& rhs) = delete;

private:
  IoStreamPosix m_writeEnd;
  IoStreamPosix m_readEnd;

  PipePosix();

  friend class IPipe;
  friend class ChildProcessPosix;
};

}       // namespace
#endif  // _RGLIB_PIPE_POSIX_HPP_
