/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>
#include <errno.h>

#include "SocketClientPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
SocketClientPosix::SocketClientPosix(FileDescriptor fd):
    m_ioStream(fd)
{
}

//------------------------------------------------------------------------------
SocketClientPosix::~SocketClientPosix()
{
  close();
}

//------------------------------------------------------------------------------
FileDescriptor SocketClientPosix::getFileDescriptor()
{
  return m_ioStream.getFileDescriptor();
}

//------------------------------------------------------------------------------
int SocketClientPosix::write(const char* buffer, unsigned int size)
{
  return m_ioStream.write(buffer, size);
}

//------------------------------------------------------------------------------
int SocketClientPosix::read(char* buffer, unsigned int size)
{
  return m_ioStream.read(buffer, size);
}

//------------------------------------------------------------------------------
bool SocketClientPosix::isOpen() const
{
  return m_ioStream.isOpen();
}

//------------------------------------------------------------------------------
void SocketClientPosix::close()
{
  return m_ioStream.close();
}

}       // namespace
