/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ThreadPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void* rglib_pthread_start(void* data)
{
  ThreadPosix::PThreadInfo* threadInfo =
      static_cast<ThreadPosix::PThreadInfo*>(data);

  threadInfo->threaded->run();
  threadInfo->isRunning = false;

  return nullptr;
}

//------------------------------------------------------------------------------
void IThread::create(std::unique_ptr<IThread>& thread)
{
  thread.reset(new ThreadPosix());
}

//------------------------------------------------------------------------------
ThreadPosix::ThreadPosix()
{
  m_threadInfo.isRunning = false;
  m_threadInfo.threaded = nullptr;
}

//------------------------------------------------------------------------------
ThreadPosix::~ThreadPosix()
{
  stop();
}

//------------------------------------------------------------------------------
int ThreadPosix::start(IThreaded& threaded)
{
  pthread_attr_t attr;

  if(isRunning())
    return -1;

  if(pthread_attr_init(&attr))
    return -1;

  m_threadInfo.threaded = &threaded;
  m_threadInfo.isRunning = true;
  if(pthread_create(&m_pThread, &attr, rglib_pthread_start, &m_threadInfo))
  {
    m_threadInfo.isRunning = false;
    return -1;
  }

  return 0;
}
//------------------------------------------------------------------------------
int ThreadPosix::stop()
{
  if(!isRunning())
    return 0;

  if(pthread_cancel(m_pThread))
    return -1;

  if(wait())
    return -1;

  m_threadInfo.isRunning = false;

  return 0;
}

//------------------------------------------------------------------------------
bool ThreadPosix::isRunning() const
{
  return m_threadInfo.isRunning;
}

//------------------------------------------------------------------------------
int ThreadPosix::wait()
{
  if(!isRunning())
    return 0;

  if(pthread_join(m_pThread, nullptr))
    return -1;

  return 0;
}

}       // namespace
