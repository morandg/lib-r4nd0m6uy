/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <errno.h>

#include "SemaphorePosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void ISemaphore::create(std::unique_ptr<ISemaphore>& semaphore)
{
  semaphore.reset(new SemaphorePosix());
}

//------------------------------------------------------------------------------
SemaphorePosix::SemaphorePosix():
    m_isInitialized(false)
{
}

//------------------------------------------------------------------------------
SemaphorePosix::~SemaphorePosix()
{
  if(m_isInitialized)
    sem_destroy(&m_posixSem);
}

//------------------------------------------------------------------------------
int SemaphorePosix::init(unsigned int value)
{
  if(m_isInitialized)
    return -1;

  if(sem_init(&m_posixSem, 0, value))
    return -errno;

  m_isInitialized = true;

  return 0;
}

//------------------------------------------------------------------------------
int SemaphorePosix::wait()
{
  if(!m_isInitialized)
    return -EINVAL;

  if(sem_wait(&m_posixSem))
    return -errno;

  return 0;
}

//------------------------------------------------------------------------------
int SemaphorePosix::post()
{
  if(!m_isInitialized)
    return -EINVAL;

  if(sem_post(&m_posixSem))
    return -errno;

  return 0;
}

}       // namespace
