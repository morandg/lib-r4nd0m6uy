/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cerrno>

#include "MutexPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void IMutex::create(std::unique_ptr<IMutex>& mutex)
{
  mutex.reset(new MutexPosix());
}


//------------------------------------------------------------------------------
MutexPosix::MutexPosix():
    m_isInitialized(false)
{
}

//------------------------------------------------------------------------------
MutexPosix::~MutexPosix()
{
  if(m_isInitialized)
    pthread_mutex_destroy(&m_mtx);
}

//------------------------------------------------------------------------------
int MutexPosix::init()
{
  if(pthread_mutex_init(&m_mtx, nullptr))
    return -errno;

  m_isInitialized = true;

  return 0;
}

//------------------------------------------------------------------------------
int MutexPosix::get()
{
  if(!m_isInitialized)
    return -EINVAL;

  if(pthread_mutex_lock(&m_mtx))
    return -errno;

  return 0;
}

//------------------------------------------------------------------------------
int MutexPosix::release()
{
  if(!m_isInitialized)
    return -EINVAL;

  if(pthread_mutex_unlock(&m_mtx))
    return -errno;

  return 0;
}

}       // namespace
