/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_SEMAPHORE_POSIX_HPP_
#define _RGLIB_SEMAPHORE_POSIX_HPP_

#include <semaphore.h>

#include "rglib/threads/ISemaphore.hpp"

namespace rglib {

class SemaphorePosix:
    public ISemaphore
{
public:
  SemaphorePosix();
  virtual ~SemaphorePosix();

  // ISemaphore
  virtual int init(unsigned int value) override;
  virtual int wait() override;
  virtual int post() override;

  SemaphorePosix(const SemaphorePosix& rhs) = delete;
  SemaphorePosix& operator=(const SemaphorePosix& rhs) = delete;

private:
  bool m_isInitialized;
  sem_t m_posixSem;
};

}       // namespace
#endif  // _RGLIB_SEMAPHORE_POSIX_HPP_
