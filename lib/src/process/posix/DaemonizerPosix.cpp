/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <sstream>
#include <cerrno>

#include "rglib/io/IFile.hpp"
#include "DaemonizerPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void IDaemonizer::create(std::unique_ptr<IDaemonizer>& daemonizer)
{
  daemonizer.reset(new DaemonizerPosix());
}

//------------------------------------------------------------------------------
DaemonizerPosix::DaemonizerPosix()
{
}

//------------------------------------------------------------------------------
DaemonizerPosix::~DaemonizerPosix()
{
}

//------------------------------------------------------------------------------
void DaemonizerPosix::setPidFile(const std::string& pidFile)
{
  m_pidFile = pidFile;
}

//------------------------------------------------------------------------------
int DaemonizerPosix:: daemonize()
{
  pid_t pid;

  pid = fork();

  if(pid == -1)
    return -errno;
  else if(pid == 0)
  {
    // Child process
    fclose(stdin);
    fclose(stdout);
    fclose(stderr);
    if(setsid() < 0)
      return -errno;

    // We don't care if writing PID file fails
    writePidFile();
    return 0;
  }

  return pid;
}

//------------------------------------------------------------------------------
void DaemonizerPosix::removePidFile()
{
  if(m_pidFile == "")
    return;

  remove(m_pidFile.c_str());
}

//------------------------------------------------------------------------------
int DaemonizerPosix::writePidFile()
{
  int err;
  std::stringstream pidValue;
  std::unique_ptr<IFile> pidFile;

  IFile::create(pidFile);

  err = pidFile->open(
      m_pidFile,
      OpenFlag::WRITE_ONLY | OpenFlag::CREATE | OpenFlag::TRUNC);
  if(err)
    return err;

  pidValue << getpid() << std::endl;

  err = pidFile->write(pidValue.str().c_str(), pidValue.str().size());
  if(err < 0)
    return err;

  return 0;
}

}       // namespace
