/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>

#include <list>
#include <cerrno>
#include <cstring>

#include "rglib/lexical/WordTokenizer.hpp"
#include "ChildProcessPosix.hpp"

namespace rglib {

static int FD_STDIN     = 0;
static int FD_STDOUT    = 1;

//------------------------------------------------------------------------------
void IChildProcess::create(std::unique_ptr<IChildProcess>& process)
{
  process.reset(new ChildProcessPosix());
}

//------------------------------------------------------------------------------
ChildProcessPosix::ChildProcessPosix():
    m_childPid(-1)
{
}

//------------------------------------------------------------------------------
ChildProcessPosix::~ChildProcessPosix()
{
}

//------------------------------------------------------------------------------
int ChildProcessPosix::start(const std::string& cmd)
{
  int ret = 0;
  pid_t forkRes;

  if(isRunning())
    return 0;

  ret = openPipes();
  if(ret)
    return ret;

  forkRes = fork();
  if(forkRes < 0)
  {
    ret = -errno;
    closePipes();
  }
  else if(forkRes > 0)
  {
    m_childPid = forkRes;
    m_stdIn.getReadEnd().close();
    m_stdOut.getWriteEnd().close();
    // XXX: stderr
  }
  else
  {
    std::list<std::string> args;
    int cArgsPos;

    dup2(m_stdOut.getWriteEnd().getFileDescriptor(), FD_STDOUT);
    dup2(m_stdIn.getReadEnd().getFileDescriptor(), FD_STDIN);
    m_stdIn.getWriteEnd().close();
    m_stdOut.getReadEnd().close();
    // XXX: stderr

    WordTokenizer::split(cmd, ' ', args);
    const char* cArgs[args.size() + 1];
    cArgsPos = 0;
    for(auto& crtArg: args)
      cArgs[cArgsPos++] = crtArg.c_str();
    cArgs[args.size()] = 0;

    execv(cArgs[0], (char**)cArgs);
    exit(-errno);
  }

  return ret;
}

//------------------------------------------------------------------------------
bool ChildProcessPosix::isRunning()
{
  if(m_childPid > 0)
  {
    int wstatus;
    int waitStatus;

    waitStatus = waitpid(m_childPid, &wstatus, WNOHANG);
    if(waitStatus == -1)
    {
      if(errno == ECHILD)
        m_childPid = -1;

      return false;
    }
    else if(waitStatus != 0)
    {
      if(WIFEXITED(wstatus) ||
          WIFSIGNALED(wstatus))
      {
        m_childPid = -1;
        return false;
      }
    }

    return true;
  }

  return false;
}

//------------------------------------------------------------------------------
int ChildProcessPosix::stop()
{
  return kill(SIGTERM);
}

//------------------------------------------------------------------------------
int ChildProcessPosix::kill(int signal)
{
  if(m_childPid != -1 && ::kill(m_childPid, signal))
    return -errno;

  closePipes();

  return 0;
}

//------------------------------------------------------------------------------
IIoStream& ChildProcessPosix::getStdIn()
{
  return m_stdIn.getWriteEnd();
}

//------------------------------------------------------------------------------
IIoStream& ChildProcessPosix::getStdOut()
{
  return m_stdOut.getReadEnd();
}

//------------------------------------------------------------------------------
int ChildProcessPosix::openPipes()
{
  int ret;

  ret = m_stdIn.open();
  if(ret == 0)
    ret = m_stdOut.open();

  if(ret)
    closePipes();

  return ret;
}

//------------------------------------------------------------------------------
void ChildProcessPosix::closePipes()
{
  m_stdIn.close();
  m_stdOut.close();
}

}       // namespace
