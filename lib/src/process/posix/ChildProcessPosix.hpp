/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_CHILD_PROCESS_POSIX_HPP_
#define _RGLIB_CHILD_PROCESS_POSIX_HPP_

#include "../../ipc/posix/PipePosix.hpp"
#include "rglib/process/IChildProcess.hpp"

namespace rglib {

class ChildProcessPosix:
    public IChildProcess
{
public:
  virtual ~ChildProcessPosix();

  virtual int start(const std::string& cmd) override;
  virtual bool isRunning() override;
  virtual int stop() override;
  virtual int kill(int signal) override;
  virtual IIoStream& getStdIn() override;
  virtual IIoStream& getStdOut() override;

  ChildProcessPosix(const ChildProcessPosix& rhs) = delete;
  ChildProcessPosix& operator=(const ChildProcessPosix& rhs) = delete;

private:
  pid_t m_childPid;
  PipePosix m_stdIn;
  PipePosix m_stdOut;

  int openPipes();
  void closePipes();

  ChildProcessPosix();

  friend class IChildProcess;
};

}       // namespace
#endif  // _RGLIB_CHILD_PROCESS_POSIX_HPP_
