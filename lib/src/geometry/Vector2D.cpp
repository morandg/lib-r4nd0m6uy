/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rglib/geometry/Vector2D.hpp"

namespace rglib {

//------------------------------------------------------------------------------
Vector2D::Vector2D():
    m_x(0),
    m_y(0)
{
}

//------------------------------------------------------------------------------
Vector2D::Vector2D(int x, int y):
    m_x(x),
    m_y(y)
{
}

//------------------------------------------------------------------------------
Vector2D::~Vector2D()
{
}

//------------------------------------------------------------------------------
int Vector2D::getX() const
{
  return m_x;
}

//------------------------------------------------------------------------------
int Vector2D::getY() const
{
  return m_y;
}

}       // namespace
