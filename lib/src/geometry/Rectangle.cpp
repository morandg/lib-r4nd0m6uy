/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rglib/geometry/Rectangle.hpp"

namespace rglib {

//------------------------------------------------------------------------------
Rectangle::Rectangle()
{
}

//------------------------------------------------------------------------------
Rectangle::Rectangle(int x, int y, int width, int height):
  m_position(x, y),
  m_size(width, height)
{

}

//------------------------------------------------------------------------------
Rectangle::~Rectangle()
{
}

//------------------------------------------------------------------------------
int Rectangle::getX() const
{
  return m_position.getX();
}

//------------------------------------------------------------------------------
int Rectangle::getY() const
{
  return m_position.getY();
}

//------------------------------------------------------------------------------
int Rectangle::getWidth() const
{
  return m_size.getX();
}

//------------------------------------------------------------------------------
int Rectangle::getHeight() const
{
  return m_size.getY();
}

//------------------------------------------------------------------------------
bool Rectangle::isIn(const Vector2D& point) const
{
  return point.getY() <= getY() &&
      point.getY() >= getY() - getHeight() &&
      point.getX() >= getX() &&
      point.getX() <= getX() + getWidth();
}

}       // namespace
