/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>

#include <cerrno>

#include "IoStreamPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
IoStreamPosix::IoStreamPosix():
    m_fd(FD_INVALID)
{
}
//------------------------------------------------------------------------------
IoStreamPosix::IoStreamPosix(FileDescriptor fd):
    m_fd(fd)
{
}

//------------------------------------------------------------------------------
IoStreamPosix::~IoStreamPosix()
{
  close();
}

//------------------------------------------------------------------------------
void IoStreamPosix::setFileDescriptor(FileDescriptor fd)
{
  m_fd = fd;
}

//------------------------------------------------------------------------------
FileDescriptor IoStreamPosix::getFileDescriptor()
{
  return m_fd;
}

//------------------------------------------------------------------------------
int IoStreamPosix::write(const char* buffer, unsigned int size)
{
  int ret;

  ret = ::write(m_fd, buffer, size);
  if(ret < 0)
    return -errno;

  return ret;
}

//------------------------------------------------------------------------------
int IoStreamPosix::read(char* buffer, unsigned int size)
{
  int ret;

  ret = ::read(m_fd, buffer, size);
  if(ret < 0)
    return -errno;

  return ret;
}

//------------------------------------------------------------------------------
bool IoStreamPosix::isOpen() const
{
  return m_fd != IIoStream::FD_INVALID;
}

//------------------------------------------------------------------------------
void IoStreamPosix::close()
{
  if(m_fd != IIoStream::FD_INVALID)
  {
    ::close(m_fd);
    m_fd = IIoStream::FD_INVALID;
  }
}

}       // namespace
