/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>

#include <cerrno>

#include "FilePosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void IFile::create(std::unique_ptr<IFile>& file)
{
  file.reset(new FilePosix());
}

//------------------------------------------------------------------------------
FilePosix::FilePosix():
    m_ioStream(FD_INVALID)
{
}

//------------------------------------------------------------------------------
FilePosix::~FilePosix()
{
  m_ioStream.close();
}

//------------------------------------------------------------------------------
int FilePosix::open(const std::string& path, int flags)
{
  FileDescriptor fd;
  int posixFlags = 0;

  if(m_ioStream.getFileDescriptor() != FD_INVALID)
    close();

  if(flags & rglib::OpenFlag::READ_WRITE)
    posixFlags |= O_RDWR;
  if(flags & rglib::OpenFlag::READ_ONLY)
    posixFlags |= O_RDONLY;
  if(flags & rglib::OpenFlag::WRITE_ONLY)
    posixFlags |= O_WRONLY;
  if(flags & rglib::OpenFlag::CREATE)
    posixFlags |= O_CREAT;
  if(flags & rglib::OpenFlag::TRUNC)
    posixFlags |= O_TRUNC;
  if(flags & rglib::OpenFlag::APPEND)
    posixFlags |= O_APPEND;

  fd = ::open(path.c_str(), posixFlags, 0664);

  if(fd < 0)
    return -errno;

  m_ioStream.setFileDescriptor(fd);

  return 0;
}

//------------------------------------------------------------------------------
FileDescriptor FilePosix::getFileDescriptor()
{
  return m_ioStream.getFileDescriptor();
}

//------------------------------------------------------------------------------
int FilePosix::write(const char* buffer, unsigned int size)
{
  return m_ioStream.write(buffer, size);
}

//------------------------------------------------------------------------------
int FilePosix::read(char* buffer, unsigned int size)
{
  return m_ioStream.read(buffer, size);
}

//------------------------------------------------------------------------------
bool FilePosix::isOpen() const
{
  return m_ioStream.isOpen();
}

//------------------------------------------------------------------------------
void FilePosix::close()
{
  return m_ioStream.close();
}

}       // namespace

