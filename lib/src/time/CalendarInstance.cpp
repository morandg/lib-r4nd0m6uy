/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include "rglib/time/CalendarInstance.hpp"

#ifndef _IS_POSIX_
#include "rglib/time/null/CalendarNull.hpp"
#endif

namespace rglib {

//------------------------------------------------------------------------------
CalendarInstance& CalendarInstance::getInstance()
{
  static CalendarInstance instance;
  return instance;
}

//------------------------------------------------------------------------------
CalendarInstance::CalendarInstance()
{
}

//------------------------------------------------------------------------------
CalendarInstance::~CalendarInstance()
{
}

//------------------------------------------------------------------------------
ICalendar& CalendarInstance::get()
{
  if(!m_calendar)
  {
#ifdef _IS_POSIX_   // XXX: Other platform need to provide factory method
    ICalendar::create(m_calendar);
#else
    m_calendar.reset(new CalendarNull());
#endif
    assert(m_calendar); // Should not happen
  }

  return *m_calendar;
}

//------------------------------------------------------------------------------
void CalendarInstance::set(std::unique_ptr<ICalendar>& calendar)
{
  m_calendar = std::move(calendar);
}

}       // namespace
