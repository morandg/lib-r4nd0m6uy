/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cerrno>

#include "CalendarPosix.hpp"

namespace rglib {

//------------------------------------------------------------------------------
void ICalendar::create(std::unique_ptr<ICalendar>& calendar)
{
  calendar.reset(new CalendarPosix());
}

//------------------------------------------------------------------------------
CalendarPosix::CalendarPosix()
{
}

//------------------------------------------------------------------------------
CalendarPosix::~CalendarPosix()
{
}

//------------------------------------------------------------------------------
int CalendarPosix::getUtcTime(TimeDuration& now)
{
  struct timespec nowPosix;

  if(clock_gettime(CLOCK_REALTIME, &nowPosix) < 0)
    return -errno;

  now = TimeDuration(nowPosix.tv_sec, nowPosix.tv_nsec);

  return 0;
}

//------------------------------------------------------------------------------
int CalendarPosix::getMonotonicTime(TimeDuration& now)
{
  struct timespec nowPosix;

  if(clock_gettime(CLOCK_MONOTONIC, &nowPosix) < 0)
    return -errno;

  now = TimeDuration(nowPosix.tv_sec, nowPosix.tv_nsec);

  return 0;
}

}       // namespace
