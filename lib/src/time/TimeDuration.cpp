/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sstream>
#include <iomanip>

#include "rglib/time/TimeDuration.hpp"

namespace rglib {

const uint64_t TimeDuration::NANO_PER_MS          = 1000000;
const uint64_t TimeDuration::NANO_PER_SECOND      = 1000000000;
const uint64_t TimeDuration::SECONDS_PER_MINUTE   = 60;
const uint64_t TimeDuration::SECONDS_PER_HOUR     = 3600;
const uint64_t TimeDuration::SECONDS_PER_DAY      = 86400;

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ZERO()
{
  static TimeDuration timeDuration(0, 0);
  return timeDuration;
}

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ONE_MILLISECOND()
{
  static TimeDuration timeDuration(0, 1000000);
  return timeDuration;
}

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ONE_SECOND()
{
  static TimeDuration timeDuration(1, 0);
  return timeDuration;
}

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ONE_MINUTE()
{
  static TimeDuration timeDuration(SECONDS_PER_MINUTE, 0);
  return timeDuration;
}

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ONE_HOUR()
{
  static TimeDuration timeDuration(SECONDS_PER_HOUR, 0);
  return timeDuration;
}

//------------------------------------------------------------------------------
const TimeDuration& TimeDuration::ONE_DAY()
{
  static TimeDuration timeDuration(SECONDS_PER_DAY, 0);
  return timeDuration;
}

//------------------------------------------------------------------------------
TimeDuration::TimeDuration():
  m_sec(0),
  m_nsec(0),
  m_isPositive(true)
{
}

//------------------------------------------------------------------------------
TimeDuration::TimeDuration(uint64_t sec, uint64_t nsec, bool isPositive):
  m_sec(sec),
  m_nsec(nsec),
  m_isPositive(isPositive)
{
  if(nsec >= NANO_PER_SECOND)
  {
    m_sec += nsec / NANO_PER_SECOND;
    m_nsec = nsec % NANO_PER_SECOND;
  }
}

//------------------------------------------------------------------------------
TimeDuration::~TimeDuration()
{
}

//------------------------------------------------------------------------------
uint64_t TimeDuration::getSeconds() const
{
  return m_sec;
}

//------------------------------------------------------------------------------
uint64_t TimeDuration::getNseconds() const
{
  return m_nsec;
}

//------------------------------------------------------------------------------
bool TimeDuration::isPositive() const
{
  return m_isPositive;
}

//------------------------------------------------------------------------------
std::string TimeDuration::toString() const
{
  std::stringstream ss;

  if(!isPositive())
    ss << "-";

  ss << getSeconds() << "." <<
      std::setfill('0') << std::setw(9) << getNseconds();

  return ss.str();
}

//------------------------------------------------------------------------------
double TimeDuration::toDouble() const
{
  double ret = (double)m_sec + ((double)m_nsec / NANO_PER_SECOND);

  if(!m_isPositive)
    ret *= -1;

  return ret;
}

//------------------------------------------------------------------------------
bool TimeDuration::operator==(const TimeDuration& rhs) const
{
  return isPositive() == rhs.isPositive() &&
      getSeconds() == rhs.getSeconds() &&
      getNseconds() == rhs.getNseconds();
}

//------------------------------------------------------------------------------
bool TimeDuration::operator!=(const TimeDuration& rhs) const
{
  return !(*this == rhs);
}

//------------------------------------------------------------------------------
bool TimeDuration::operator<(const TimeDuration& rhs) const
{
  if(isPositive() && rhs.isPositive())
  {
    if(getSeconds() < rhs.getSeconds())
      return true;
    if(getSeconds() == rhs.getSeconds() &&
        getNseconds() < rhs.getNseconds())
      return true;

    return false;
  }

  if(!isPositive() && !rhs.isPositive())
  {
    if(getSeconds() > rhs.getSeconds())
      return true;
    if(getSeconds() == rhs.getSeconds() &&
        getNseconds() > rhs.getNseconds())
      return true;

    return false;
  }

  if(isPositive())
    return false;
  else
    return true;
}

//------------------------------------------------------------------------------
bool TimeDuration::operator<=(const TimeDuration& rhs) const
{
  return *this < rhs || *this == rhs;
}

//------------------------------------------------------------------------------
bool TimeDuration::operator>(const TimeDuration& rhs) const
{
  return rhs < *this;
}

//------------------------------------------------------------------------------
bool TimeDuration::operator>=(const TimeDuration& rhs) const
{
  return *this > rhs || *this == rhs;
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator+(const TimeDuration& rhs) const
{
  if(isPositive() == rhs.isPositive())
    return TimeDuration(
        getSeconds() + rhs.getSeconds(),
        getNseconds() + rhs.getNseconds(),
        isPositive());

  // x + (-y) => x - y
  if(isPositive())
    return *this - TimeDuration(rhs.getSeconds(), rhs.getNseconds());

  // (-x) + y => y - x
  return rhs - TimeDuration(getSeconds(), getNseconds());
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator+=(const TimeDuration& rhs)
{
  *this = *this + rhs;
  return *this;
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator-(const TimeDuration& rhs) const
{
  if(isPositive() == rhs.isPositive())
  {
    if(isPositive())
    {
      // Result will be negative
      if(*this < rhs)
      {
        // small - big = -(big - small)
        TimeDuration temp = rhs - *this;
        return TimeDuration(temp.getSeconds(), temp.getNseconds(), false);
      }

      // Result will be positive
      else
      {
        uint64_t sec;
        uint64_t nsec;

        if(getNseconds() < rhs.getNseconds())
        {
          sec = getSeconds() - rhs.getSeconds() - 1;
          nsec = getNseconds() - rhs.getNseconds() + NANO_PER_SECOND;
        }
        else
        {
          sec = getSeconds() - rhs.getSeconds();
          nsec = getNseconds() - rhs.getNseconds();
        }
        return TimeDuration(sec, nsec, true);
      }
    }
    else
    {
      if(*this < rhs)
      {
        // -big - (-small) = -(big - small)
        TimeDuration temp = TimeDuration(rhs.getSeconds(), rhs.getNseconds()) -
            TimeDuration(getSeconds(), getNseconds());
        return TimeDuration(temp.getSeconds(), temp.getNseconds(), false);
      }
      else
      {
        // -small - (-big) = big - small
        return TimeDuration(rhs.getSeconds(), rhs.getNseconds())-
            TimeDuration(getSeconds(), getNseconds());
      }
    }
  }

  // x - (-y) => x + y
  if(isPositive())
    return *this + TimeDuration(rhs.getSeconds() , rhs.getNseconds());

  // (-x) - y => -(x + y)
  TimeDuration temp = TimeDuration(getSeconds(), getNseconds()) + rhs;
  return TimeDuration(temp.getSeconds(), temp.getNseconds(), false);
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator-=(const TimeDuration& rhs)
{
  *this = *this - rhs;

  return *this;
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator*(const int& rhs) const
{
  return TimeDuration(getSeconds() * rhs, getNseconds() * rhs);
}

//------------------------------------------------------------------------------
TimeDuration TimeDuration::operator*=(const int& rhs)
{
  *this = *this * rhs;

  return *this;
}

//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, const TimeDuration& duration)
{
  os << duration.toString();

  return os;
}

}       // namespace
