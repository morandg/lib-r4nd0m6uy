/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>

#include "LoggerLog4CPlus.hpp"

namespace rglib {

//------------------------------------------------------------------------------
LoggerLog4CPlus::LoggerLog4CPlus()
{
}

//------------------------------------------------------------------------------
LoggerLog4CPlus::~LoggerLog4CPlus()
{
}

//------------------------------------------------------------------------------
int LoggerLog4CPlus::init(const std::string& config)
{
  log4cplus::initialize();
  log4cplus::PropertyConfigurator::doConfigure(config);
  m_log4CplusLogger = log4cplus::Logger::getRoot();

  return 0;
}

//------------------------------------------------------------------------------
void LoggerLog4CPlus::sinkLogLine(LogLevel level, const std::string& line)
{
  if(level == LogLevel::ERROR)
  {
    LOG4CPLUS_ERROR(m_log4CplusLogger, line);
  }
  else if(level == LogLevel::WARNING)
  {
    LOG4CPLUS_WARN(m_log4CplusLogger, line);
  }
  else if(level == LogLevel::INFO)
  {
    LOG4CPLUS_INFO(m_log4CplusLogger, line);
  }
  else
  {
    LOG4CPLUS_DEBUG(m_log4CplusLogger, line);
  }
}

}       // namespace
