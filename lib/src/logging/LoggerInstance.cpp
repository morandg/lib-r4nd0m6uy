/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "null/LoggerNull.hpp"
#include "console/LoggerConsole.hpp"
#ifdef _HAS_LOG4CPLUS_
#include "log4cplus/LoggerLog4CPlus.hpp"
#endif
#include "rglib/logging/LoggerInstance.hpp"

namespace rglib {

//------------------------------------------------------------------------------
LoggerInstance& LoggerInstance::getInstance()
{
  static LoggerInstance theInstance;
  return theInstance;
}

//------------------------------------------------------------------------------
LoggerInstance::LoggerInstance()
{
}

//------------------------------------------------------------------------------
LoggerInstance::~LoggerInstance()
{
}

//------------------------------------------------------------------------------
int LoggerInstance::defaultLogger()
{
  return consoleLogger();
}

//------------------------------------------------------------------------------
int LoggerInstance::consoleLogger()
{
  m_logger.reset(new LoggerConsole());

  return 0;
}

//------------------------------------------------------------------------------
int LoggerInstance::nullLogger()
{
  m_logger.reset(new LoggerNull());

  return 0;
}

//------------------------------------------------------------------------------
int LoggerInstance::log4cplus(const std::string& config)
{
#ifdef _HAS_LOG4CPLUS_
  std::unique_ptr<LoggerLog4CPlus> log4cplus(
      new LoggerLog4CPlus());

  if(log4cplus->init(config))
    return -1;

  m_logger = std::move(log4cplus);

  // Max level configuration is done in log4cplus config file
  setMaxLevel(ILogger::DEBUG);

  return 0;
#else
  (void)config;
#endif

  std::cerr << "No log4cplus support on this system!" << std::endl;

  return -1;
}

//------------------------------------------------------------------------------
void LoggerInstance::setMaxLevel(ILogger::LogLevel level)
{
  if(!m_logger)
    defaultLogger();

  m_logger->setMaxLevel(level);
}

//------------------------------------------------------------------------------
ILogger& LoggerInstance::operator*()
{
  if(!m_logger)
    defaultLogger();

//  std::cout << "getLogger " << (int)m_logger.get() << std::endl;

  return *m_logger;
}

}       // namespace
