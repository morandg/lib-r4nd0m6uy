/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rglib/lexical/TokenizerString.hpp"
#include "rglib/lexical/WordTokenizer.hpp"

namespace rglib {

//------------------------------------------------------------------------------
WordTokenizer::WordTokenizer(ITokenizer& tokenizer):
    m_tokenizer(tokenizer)
{
}

//------------------------------------------------------------------------------
WordTokenizer::~WordTokenizer()
{
}

//------------------------------------------------------------------------------
void WordTokenizer::split(
    const std::string buffer, char separator, std::list<std::string>& words)
{
  TokenizerString tokenizer;
  WordTokenizer splitter(tokenizer);

  tokenizer.setBuffer(buffer);
  splitter.split(separator, words);
}

//------------------------------------------------------------------------------
bool WordTokenizer::getNextWord(char separator, std::string& word)
{
  char token;

  word = "";

  while(m_tokenizer.nextToken(token))
  {
    if(token == separator)
      return true;

    word += token;
  }

  return false;
}

//------------------------------------------------------------------------------
void WordTokenizer::split(char separator, std::list<std::string>& words)
{
  std::string foundWord;

  words.clear();

  while(getNextWord(separator, foundWord))
    if(foundWord != "")
      words.push_back(foundWord);

  words.push_back(foundWord);
}

}       // namespace
