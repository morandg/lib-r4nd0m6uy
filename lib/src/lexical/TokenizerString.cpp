/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rglib/lexical/TokenizerString.hpp"

namespace rglib {

//------------------------------------------------------------------------------
TokenizerString::TokenizerString():
    m_buffPos(0)
{
}

//------------------------------------------------------------------------------
TokenizerString::~TokenizerString()
{
}

//------------------------------------------------------------------------------
void TokenizerString::setBuffer(const std::string& buffer)
{
  m_buffPos = 0;
  m_buffer = buffer;
}

//------------------------------------------------------------------------------
bool TokenizerString::hasToken() const
{
  return m_buffPos < m_buffer.size();
}

//------------------------------------------------------------------------------
bool TokenizerString::nextToken(char& token)
{
  if(!hasToken())
    return false;

  token = m_buffer[m_buffPos];
  ++m_buffPos;

  return true;
}

}       // namespace
