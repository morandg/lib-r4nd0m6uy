# R4nd0m6uy's libs
A small collection of reusable C++ objects for platform abstraction, I
have been writing everytime I stared a new project.

Everything should be mockable with the
[CppUTest](https://cpputest.github.io/) testing library.

# Credits
* [Tiny test library](https://github.com/joewalnes/tinytest)
* Logging framework inspired from [Dr Doobs' article](http://www.drdobbs.com/cpp/logging-in-c/201804215)
