/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TESTS_FILE_MOCK_HPP_
#define _RGLIB_TESTS_FILE_MOCK_HPP_

#include <rglib/io/IFile.hpp>

namespace rglib {
namespace mocks {

class FileMock:
    public IFile
{
public:
  FileMock();
  virtual ~FileMock();

  virtual int open(const std::string& path, int flags) override;
  virtual FileDescriptor getFileDescriptor() override;
  virtual int write(const char* buffer, unsigned int size) override;
  virtual int read(char* buffer, unsigned int size) override;
  virtual bool isOpen() const override;
  virtual void close() override;
};

}}      // namespace
#endif  // _RGLIB_TESTS_FILE_MOCK_HPP_
