/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTestExt/MockSupport.h>

#include "TimerMock.hpp"

namespace rglib {
namespace mocks {

//------------------------------------------------------------------------------
TimerMock::TimerMock()
{
}

//------------------------------------------------------------------------------
TimerMock::~TimerMock()
{
}

//------------------------------------------------------------------------------
const std::string& TimerMock::getName() const
{
  return *((std::string*)mock().actualCall(__func__).onObject(this).
      returnPointerValue());
}

//------------------------------------------------------------------------------
void TimerMock::start()
{
  mock().actualCall(__func__).onObject(this);
}

//------------------------------------------------------------------------------
void TimerMock::stop()
{
  mock().actualCall(__func__).onObject(this);
}

//------------------------------------------------------------------------------
void TimerMock::restart()
{
  mock().actualCall(__func__).onObject(this);
}

//------------------------------------------------------------------------------
bool TimerMock::isRunning()
{
  return mock().actualCall(__func__).onObject(this).
      returnBoolValue();
}

//------------------------------------------------------------------------------
void TimerMock::setTimeout(unsigned int ms)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("ms", ms);
}

}}      // namespace
