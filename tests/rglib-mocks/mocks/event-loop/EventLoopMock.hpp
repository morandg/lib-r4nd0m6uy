/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TESTS_EVENT_LOOP_MOCK_HPP_
#define _RGLIB_TESTS_EVENT_LOOP_MOCK_HPP_

#include <memory>

#include <rglib/event-loop/IEventLoop.hpp>

namespace rglib {
namespace mocks {

class EventLoopMock:
    public IEventLoop
{
public:
  EventLoopMock();
  virtual ~EventLoopMock();

  // IEventLoop
  virtual int init() override;
  virtual int reinit() override;
  virtual int run() override;
  virtual int breakLoop() override;
  virtual int createTimer(
      std::unique_ptr<ITimer>& timer,
      const std::string& name,
      ITimedOut& timedOut)  override;
  virtual int addHandledSignal(
      IHandledSignal& handler, SignalHandle signal) override;
  virtual int addHandledIo(IHandledIo& handler, int what) override;
  virtual void removeHandledIo(IHandledIo& handler) override;
};

}}      // namespace
#endif  // _RGLIB_TESTS_EVENT_LOOP_MOCK_HPP_
