/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTestExt/MockSupport.h>

#include "EventLoopMock.hpp"

namespace rglib {
namespace mocks {

//------------------------------------------------------------------------------
EventLoopMock::EventLoopMock()
{
}

//------------------------------------------------------------------------------
EventLoopMock::~EventLoopMock()
{
}

//------------------------------------------------------------------------------
int EventLoopMock::init()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::reinit()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::run()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::breakLoop()
{
  return mock().actualCall(__func__).onObject(this).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::createTimer(
      std::unique_ptr<ITimer>& timer,
      const std::string& name,
      ITimedOut& timedOut)
{
  return mock().actualCall(__func__).onObject(this).
      withOutputParameterOfType("std::unique_ptr<ITimer>", "timer", &timer).
      withParameterOfType("std::string", "name", &name).
      withParameter("timedOut", &timedOut).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::addHandledSignal(
      IHandledSignal& handler, SignalHandle signal)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("handler", &handler).
      withParameter("signal", signal).
      returnIntValue();
}

//------------------------------------------------------------------------------
int EventLoopMock::addHandledIo(IHandledIo& handler, int what)
{
  return mock().actualCall(__func__).onObject(this).
      withParameter("handler", &handler).
      withParameter("what", what).
      returnIntValue();
}

//------------------------------------------------------------------------------
void EventLoopMock::removeHandledIo(IHandledIo& handler)
{
  mock().actualCall(__func__).onObject(this).
      withParameter("handler", &handler);
}

}}      // namespace
