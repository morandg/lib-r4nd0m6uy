/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RGLIB_TESTS_TIMER_MOCK_HPP_
#define _RGLIB_TESTS_TIMER_MOCK_HPP_

#include <rglib/event-loop/ITimer.hpp>

namespace rglib {
namespace mocks {

class TimerMock:
    public ITimer
{
public:
  TimerMock();
  virtual ~TimerMock();

  virtual const std::string& getName() const override;
  virtual void start() override;
  virtual void stop() override;
  virtual void restart() override;
  virtual bool isRunning() override;
  virtual void setTimeout(unsigned int ms) override;
};

}}      // namespace
#endif  // _RGLIB_TESTS_TIMER_MOCK_HPP_
