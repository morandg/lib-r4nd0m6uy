/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include "tinytest/tinytest.h"

#include <rglib/threads/IThread.hpp>
#include <rglib/threads/IMutex.hpp>

//------------------------------------------------------------------------------
class MutexJob:
    public rglib::IThreaded
{
public:
  MutexJob(rglib::IMutex& mutex):
    m_hasFinishedJob(false),
    m_error(0),
    m_mutex(mutex)
  {
  };

  virtual ~MutexJob()
  {
  };

  virtual void run() override
  {
    if(m_mutex.get())
      m_error++;

    if(m_mutex.release())
      m_error++;

    m_hasFinishedJob = true;
  }

  bool hasFinishedJob()
  {
    return m_hasFinishedJob;
  }

  int getErrors()
  {
    return m_error;
  }

private:
  bool m_hasFinishedJob;
  int m_error;
  rglib::IMutex& m_mutex;
};

//------------------------------------------------------------------------------
void threadIsLocked()
{
  std::unique_ptr<rglib::IThread> lockedThread;
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);
  rglib::IThread::create(lockedThread);

  MutexJob lockedJob(*mutex);

  mutex->get();
  lockedThread->start(lockedJob);
  usleep(100000);
  ASSERT("Thread is blocked", lockedThread->isRunning());
  mutex->release();
  usleep(100000);
  ASSERT("Thread is no more blocked", !lockedThread->isRunning());
  ASSERT("Thread exited normally", lockedJob.hasFinishedJob());
  ASSERT_EQUALS(0, lockedJob.getErrors());
}

//------------------------------------------------------------------------------
void canGetAndRelease()
{
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);

  ASSERT("Can be initialized", mutex->init() == 0);
  ASSERT_EQUALS(0, mutex->get());
  ASSERT_EQUALS(0, mutex->release());
}

//------------------------------------------------------------------------------
void canBeInitialized()
{
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);

  ASSERT("Can be initialized", mutex->init() == 0);
}

//------------------------------------------------------------------------------
void cannotReleaseWhenNotInitialized()
{
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);

  ASSERT("Must be initialized before release", mutex->release() < 0);
}

//------------------------------------------------------------------------------
void cannotGetWhenNotInitialized()
{
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);

  ASSERT("Must be initialized before get", mutex->get() < 0);
}

//------------------------------------------------------------------------------
void canBeCreated()
{
  std::unique_ptr<rglib::IMutex> mutex;

  rglib::IMutex::create(mutex);

  ASSERT("Can be created", mutex);
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(canBeCreated);
  RUN(cannotGetWhenNotInitialized);
  RUN(cannotReleaseWhenNotInitialized);
  RUN(canBeInitialized);
  RUN(canGetAndRelease);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
