/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include "tinytest/tinytest.h"

#include <rglib/threads/ISemaphore.hpp>
#include <rglib/threads/IThread.hpp>

class WaitThread:
    rglib::IThreaded
{
public:
  WaitThread(rglib::ISemaphore& sem):
    m_sem(sem),
    m_waitRetCode(-1)
  {
  };

  ~WaitThread()
  {
    if(m_thread)
      m_thread->stop();
  };

  int waitInBg()
  {
    rglib::IThread::create(m_thread);
    return m_thread->start(*this);
  }

  void run()
  {
    m_waitRetCode = m_sem.wait();
  }

  bool isWaiting()
  {
    if(m_thread)
      return m_thread->isRunning();
    return false;
  }

  bool wasSuccess()
  {
    return m_waitRetCode == 0;
  }

private:
  rglib::ISemaphore& m_sem;
  int m_waitRetCode;
  std::unique_ptr<rglib::IThread> m_thread;
};

//------------------------------------------------------------------------------
void waitNoneInitialized()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);

  ASSERT_EQUALS(true, sem->wait() < 0);
}

//------------------------------------------------------------------------------
void initSemTwice()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);

  ASSERT_EQUALS(0, sem->init(0));
  ASSERT_EQUALS(-1, sem->init(0));
}

//------------------------------------------------------------------------------
void waitInitializedToOneReturns()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);

  ASSERT_EQUALS(0, sem->init(1));
  ASSERT_EQUALS(0, sem->wait());
}

//------------------------------------------------------------------------------
void waitZeroInitializedNeverEnds()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);
  WaitThread waitThread(*sem);

  ASSERT_EQUALS(0, sem->init(0));
  ASSERT_EQUALS(0, waitThread.waitInBg());
  usleep(100000);
  ASSERT_EQUALS(true, waitThread.isWaiting());
}

//------------------------------------------------------------------------------
void postNonInitialized()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);

  ASSERT_EQUALS(true, sem->post() < 0);
}

//------------------------------------------------------------------------------
void canWaitAfterPost()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);

  ASSERT_EQUALS(0, sem->init(0));
  ASSERT_EQUALS(0, sem->post());
  ASSERT_EQUALS(0, sem->wait());
}

//------------------------------------------------------------------------------
void unlockAfterPost()
{
  std::unique_ptr<rglib::ISemaphore> sem;

  rglib::ISemaphore::create(sem);
  WaitThread waitThread(*sem);

  ASSERT_EQUALS(0, sem->init(0));
  ASSERT_EQUALS(0, waitThread.waitInBg());
  usleep(100000);
  ASSERT_EQUALS(true, waitThread.isWaiting());
  ASSERT_EQUALS(0, sem->post());
  usleep(100000);
  ASSERT_EQUALS(false, waitThread.isWaiting());
  ASSERT_EQUALS(true, waitThread.wasSuccess());
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(waitNoneInitialized);
  RUN(initSemTwice);
  RUN(waitInitializedToOneReturns);
  RUN(waitZeroInitializedNeverEnds);
  RUN(postNonInitialized);
  RUN(canWaitAfterPost);
  RUN(unlockAfterPost);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
