/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include <rglib/time/CalendarInstance.hpp>

#include "tinytest/tinytest.h"

//------------------------------------------------------------------------------
void monotonicFlows()
{
  rglib::TimeDuration start;
  rglib::TimeDuration end;
  rglib::TimeDuration diff;
  int usecSleep = 10000;

  RGLIB_GET_MONOTONIC_TIME(start);
  usleep(usecSleep);
  RGLIB_GET_MONOTONIC_TIME(end);

  diff = end - start;
  ASSERT("Diff is time spent in sleep", diff.toDouble() >= 0.01);
}

//------------------------------------------------------------------------------
void monotonicIsSmallerThanUtc()
{
  rglib::TimeDuration monotonic;
  rglib::TimeDuration utc;

  RGLIB_GET_MONOTONIC_TIME(monotonic);
  RGLIB_GET_UTC_TIME(utc);

  ASSERT("Monotonic smaller than UTC", monotonic < utc);
}

//------------------------------------------------------------------------------
void canGetMonotonicTime()
{
  rglib::TimeDuration monotonic;

  RGLIB_GET_MONOTONIC_TIME(monotonic);

  ASSERT("UTC is not zero", monotonic.toDouble() > 0);
}

//------------------------------------------------------------------------------
void canGetUtcTime()
{
  rglib::TimeDuration utc;

  RGLIB_GET_UTC_TIME(utc);

  ASSERT("UTC is not zero", utc.toDouble() > 0);
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(canGetUtcTime);
  RUN(canGetMonotonicTime);
  RUN(monotonicIsSmallerThanUtc);
  RUN(monotonicFlows);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
