/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include <rglib/io/IFile.hpp>

#include "tinytest/tinytest.h"

static const std::string TEST_FILE = "testFile";

//------------------------------------------------------------------------------
void defaultConstructorHasInvalidFd()
{
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  ASSERT_EQUALS(rglib::IIoStream::FD_INVALID, file->getFileDescriptor());
}

//------------------------------------------------------------------------------
void defaultConstructorIsNotOpen()
{
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  ASSERT_EQUALS(false, file->isOpen());
}

//------------------------------------------------------------------------------
void openIsOpen()
{
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  ASSERT_EQUALS(
      0, file->open(
          TEST_FILE,
          rglib::OpenFlag::CREATE | rglib::OpenFlag::READ_WRITE)
      );
  ASSERT_EQUALS(true, file->isOpen());
}

//------------------------------------------------------------------------------
void openAndCloseFileDescriptorInvalid()
{
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE | rglib::OpenFlag::READ_WRITE);
  file->close();

  ASSERT_EQUALS(rglib::IIoStream::FD_INVALID, file->getFileDescriptor());
}

//------------------------------------------------------------------------------
void writeClosedFile()
{
  int dataSize = 8;
  char data[dataSize];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  ASSERT_EQUALS(true, file->write(data, dataSize) < 0);
}

//------------------------------------------------------------------------------
void writeFile()
{
  int dataSize = 8;
  char data[dataSize];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE | rglib::OpenFlag::WRITE_ONLY);
  ASSERT_EQUALS(dataSize, file->write(data, dataSize));
}

//------------------------------------------------------------------------------
void readClosedFile()
{
  int dataSize = 8;
  char data[dataSize];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  ASSERT_EQUALS(true, file->read(data, dataSize) < 0);
}

//------------------------------------------------------------------------------
void readWriteSameContent()
{
  int dataSize = 8;
  char dataIn[dataSize];
  char dataOut[dataSize];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  memset(dataOut, 0, dataSize);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::TRUNC);
  file->write(dataIn, dataSize);

  file->open(TEST_FILE, rglib::OpenFlag::READ_ONLY);
  ASSERT_EQUALS(dataSize, file->read(dataOut, dataSize));
  ASSERT_EQUALS(0, memcmp(dataIn, dataOut, dataSize));
}

//------------------------------------------------------------------------------
void truncatedAfterSmallerWrite()
{
  int dataSize = 8;
  char dataIn[dataSize] = "12345678";
  char dataOut[dataSize];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::TRUNC);
  file->write(dataIn, dataSize);
  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::TRUNC);
  file->write(dataIn, dataSize / 2);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE | rglib::OpenFlag::READ_ONLY);
  ASSERT_EQUALS(dataSize / 2, file->read(dataOut, dataSize));
  ASSERT_EQUALS(0, memcmp(dataIn, dataOut, dataSize / 2));
}

//------------------------------------------------------------------------------
void writeAppendToFile()
{
  int dataSize = 8;
  char dataIn[dataSize];
  char dataOut[dataSize * 2];
  std::unique_ptr<rglib::IFile> file;

  rglib::IFile::create(file);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::TRUNC);
  file->write(dataIn, dataSize);
  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE |
        rglib::OpenFlag::WRITE_ONLY |
        rglib::OpenFlag::APPEND);
  file->write(dataIn, dataSize);

  file->open(
      TEST_FILE,
      rglib::OpenFlag::CREATE | rglib::OpenFlag::READ_ONLY);
  ASSERT_EQUALS(dataSize * 2, file->read(dataOut, dataSize * 2));
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(defaultConstructorHasInvalidFd);
  RUN(defaultConstructorIsNotOpen);
  RUN(openIsOpen);
  RUN(openAndCloseFileDescriptorInvalid);
  RUN(writeClosedFile);
  RUN(writeFile);
  RUN(readClosedFile);
  RUN(readWriteSameContent);
  RUN(truncatedAfterSmallerWrite);
  RUN(writeAppendToFile);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
