/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/logging/LogMacros.hpp>

#include "tinytest/tinytest.h"

//------------------------------------------------------------------------------
void logAllLevels(const std::string& message)
{
  LOGDB() << message << " debug";
  LOGIN() << message << " info";
  LOGWA() << message << " warning";
  LOGER() << message << " error";

  ASSERT("Success" , 1);
}

//------------------------------------------------------------------------------
void testLog4Cplus()
{
  if(rglib::LoggerInstance::getInstance().
      log4cplus("tests/integration/log4cplus.ini"))
    return;

  logAllLevels("log4cplus test log");
}

//------------------------------------------------------------------------------
void testConsoleLoggerMaxDebug()
{
  rglib::LoggerInstance::getInstance().consoleLogger();
  rglib::LoggerInstance::getInstance().setMaxLevel(rglib::ILogger::DEBUG);

  logAllLevels("Hello console with max debug");

  // Need user intervention to check log message ...
  ASSERT("Success" , 1);
}

//------------------------------------------------------------------------------
void testConsoleLogger()
{
  rglib::LoggerInstance::getInstance().consoleLogger();

  logAllLevels("Hello console logger");

  // Need user intervention to check log message ...
  ASSERT("Success" , 1);
}

//------------------------------------------------------------------------------
void testDefaultLogger()
{
  rglib::LoggerInstance::getInstance().defaultLogger();

  logAllLevels("Hello default logger");

  // Need user intervention to check log message ...
  ASSERT("Success" , 1);
}

//------------------------------------------------------------------------------
void testUnsetLogger()
{
  logAllLevels("Hello unset logger");

  // Need user intervention to check log message ...
  ASSERT("Success" , 1);
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(testUnsetLogger);
  RUN(testDefaultLogger);
  RUN(testConsoleLogger);
  RUN(testConsoleLoggerMaxDebug);
  RUN(testLog4Cplus);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
