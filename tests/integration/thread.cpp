/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <unistd.h>

#include "tinytest/tinytest.h"

#include <rglib/threads/IThread.hpp>

//------------------------------------------------------------------------------
class TimedJob:
    public rglib::IThreaded
{
public:
  TimedJob(unsigned int ms):
    m_durationMs(ms),
    m_hasFinishedJob(false)
  {
  };

  virtual ~TimedJob()
  {
  };

  virtual void run() override
  {
    sleep(m_durationMs);
    m_hasFinishedJob = true;
  }

  bool hasFinishedJob()
  {
    return m_hasFinishedJob;
  }

  static void sleep(unsigned int ms)
  {
    usleep(ms * 1000);
  }

private:
  unsigned int m_durationMs;
  bool m_hasFinishedJob;
};

//------------------------------------------------------------------------------
void waitIsNoMoreRunning()
{
  std::unique_ptr<rglib::IThread> thread;
  TimedJob waitThread(100);

  rglib::IThread::create(thread);

  thread->start(waitThread);
  ASSERT_EQUALS(0, thread->wait());
  ASSERT_EQUALS(true, waitThread.hasFinishedJob());
}

//------------------------------------------------------------------------------
void waitNotRunningThread()
{
  std::unique_ptr<rglib::IThread> thread;
  TimedJob waitThread(0);

  rglib::IThread::create(thread);

  ASSERT_EQUALS(0, thread->wait());
  ASSERT_EQUALS(false, waitThread.hasFinishedJob());
}

//------------------------------------------------------------------------------
void finishedThreadIssNoMoreRunning()
{
  std::unique_ptr<rglib::IThread> thread;
  TimedJob waitThread(0);

  rglib::IThread::create(thread);

  thread->start(waitThread);
  TimedJob::sleep(5);
  ASSERT_EQUALS(false, thread->isRunning());
  ASSERT_EQUALS(true, waitThread.hasFinishedJob());
}

//------------------------------------------------------------------------------
void startedThreadCanBeStopped()
{
  std::unique_ptr<rglib::IThread> thread;
  TimedJob waitThread(500);

  rglib::IThread::create(thread);

  ASSERT_EQUALS(0, thread->start(waitThread));
  ASSERT_EQUALS(true, thread->isRunning());
  ASSERT_EQUALS(0, thread->stop());
  ASSERT_EQUALS(false, thread->isRunning());
  ASSERT_EQUALS(false, waitThread.hasFinishedJob());
}

//------------------------------------------------------------------------------
void stopANoneRunningThread()
{
  std::unique_ptr<rglib::IThread> thread;

  rglib::IThread::create(thread);

  ASSERT_EQUALS(0, thread->stop());
}

//------------------------------------------------------------------------------
void newThreadIsNotRunning()
{
  std::unique_ptr<rglib::IThread> thread;
  TimedJob waitThread(100);

  rglib::IThread::create(thread);

  ASSERT_EQUALS(false, thread->isRunning());
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(newThreadIsNotRunning);
  RUN(stopANoneRunningThread);
  RUN(startedThreadCanBeStopped);
  RUN(finishedThreadIssNoMoreRunning);
  RUN(waitNotRunningThread);
  RUN(waitIsNoMoreRunning);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
