/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rglib/ipc/IPipe.hpp>

#include "tinytest/tinytest.h"


//------------------------------------------------------------------------------
void cannotReadInWriteEnd()
{
  unsigned int buffSize = 32;
  char readBuff[buffSize];
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  pipe->open();
  ASSERT("Cannot read from write end",
      pipe->getWriteEnd().read(readBuff, buffSize) < 0);
}

//------------------------------------------------------------------------------
void cannotWriteInRead()
{
  std::string data = "Hello pipe";
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  pipe->open();
  ASSERT("Cannot write in read end",
      pipe->getReadEnd().write(data.c_str(), data.size()) < 0);
}

//------------------------------------------------------------------------------
void readWriteSameData()
{
  std::string data = "Hello pipe";
  unsigned int buffSize = 32;
  char readBuff[buffSize];
  int readRet;
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  pipe->open();
  ASSERT_EQUALS(
      (int)data.size(), pipe->getWriteEnd().write(data.c_str(), data.size()));

  readRet = pipe->getReadEnd().read(readBuff, buffSize);
  ASSERT_EQUALS((int)data.size(), readRet);
  ASSERT("Read same string", data == std::string(readBuff, readRet));
}

//------------------------------------------------------------------------------
void close()
{
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  pipe->open();
  pipe->close();

  ASSERT_EQUALS(
      rglib::IIoStream::FD_INVALID, pipe->getReadEnd().getFileDescriptor());
  ASSERT_EQUALS(
      rglib::IIoStream::FD_INVALID, pipe->getWriteEnd().getFileDescriptor());
}

//------------------------------------------------------------------------------
void openTwice()
{
  rglib::FileDescriptor fdRead;
  rglib::FileDescriptor fdWrite;
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  pipe->open();
  fdRead = pipe->getReadEnd().getFileDescriptor();
  fdWrite = pipe->getWriteEnd().getFileDescriptor();

  pipe->open();
  ASSERT_EQUALS(fdRead, pipe->getReadEnd().getFileDescriptor());
  ASSERT_EQUALS(fdWrite, pipe->getWriteEnd().getFileDescriptor());
}

//------------------------------------------------------------------------------
void canBeOpened()
{
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  ASSERT_EQUALS(0, pipe->open());
  ASSERT("Read end open", pipe->getReadEnd().isOpen());
  ASSERT("Write end open", pipe->getWriteEnd().isOpen());
}

//------------------------------------------------------------------------------
void canBeCreated()
{
  std::unique_ptr<rglib::IPipe> pipe;

  rglib::IPipe::create(pipe);

  ASSERT("Can be created", pipe.get() != nullptr);
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(canBeCreated);
  RUN(canBeOpened);
  RUN(openTwice);
  RUN(close);
  RUN(readWriteSameData);
  RUN(cannotWriteInRead);
  RUN(cannotReadInWriteEnd);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
