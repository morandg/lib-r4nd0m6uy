/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>

#include <signal.h>
#include <unistd.h>

#include <cstring>

#include <rglib/event-loop/IEventLoop.hpp>
#include <rglib/threads/IThread.hpp>
#include <rglib/time/CalendarInstance.hpp>

#include "tinytest/tinytest.h"

//------------------------------------------------------------------------------
class SignalHandler:
    public rglib::IHandledSignal
{
public:
  SignalHandler(rglib::IEventLoop& el):
    m_el(el),
    m_hadSignal(false),
    m_signal(0)
  {
  }

  ~SignalHandler()
  {
  }

  void onSignal(rglib::SignalHandle s) override
  {
    m_hadSignal = true;
    m_signal = s;

    m_el.breakLoop();
  }

  bool hadSignal()
  {
    return m_hadSignal;
  }

  rglib::SignalHandle getSignal()
  {
    return m_signal;
  }

private:
  rglib::IEventLoop& m_el;
  bool m_hadSignal;
  rglib::SignalHandle m_signal;
};

//------------------------------------------------------------------------------
class IoHandler:
    public rglib::IHandledIo
{
public:
  IoHandler(rglib::FileDescriptor handle):
    m_handle(handle),
    m_hadData(false)
  {
  }

  ~IoHandler()
  {
  }

  virtual rglib::FileDescriptor getFileDescriptor()
  {
    return m_handle;
  }

  virtual void onReadReady()
  {
    unsigned int bufferSize = 32;
    char buffer[bufferSize];
    ssize_t readSize;

    readSize = read(m_handle, buffer, bufferSize);
    m_data += std::string(buffer, readSize);

    m_hadData = true;
  }

  bool hadData()
  {
    return m_hadData;
  }

  std::string& getData()
  {
    return m_data;
  }

private:
  rglib::FileDescriptor m_handle;
  bool m_hadData;
  std::string m_data;
};

//------------------------------------------------------------------------------
class LoopThread:
    public rglib::IThreaded
{
public:
  LoopThread(rglib::IEventLoop& el):
      m_el(el),
      m_elRetCode(-1)
  {
  };

  virtual ~LoopThread()
  {
  };

  virtual void run() override
  {
    m_elRetCode = m_el.run();
  }

  virtual int getElRetCode()
  {
    return m_elRetCode;
  }

private:
  rglib::IEventLoop& m_el;
  int m_elRetCode;
};

//------------------------------------------------------------------------------
class TimerTimedOut:
    public rglib::ITimedOut
{
public:
  TimerTimedOut():
    m_hasTimedOut(false)
  {
  };

  ~TimerTimedOut()
  {
  };

  void onTimeout(const std::string& who) override
  {
    m_timerName = who;
    m_hasTimedOut = true;
  };

  bool hastimedOut()
  {
    return m_hasTimedOut;
  }

  const std::string& getTimerName() const
  {
    return m_timerName;
  }

private:
  bool m_hasTimedOut;
  std::string m_timerName;
};

//------------------------------------------------------------------------------
void timerTimesOutWithRightDelay()
{
  rglib::TimeDuration start;
  rglib::TimeDuration end;
  TimerTimedOut timedOut;
  std::unique_ptr<rglib::ITimer> timer;
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IEventLoop::create(el);

  el->init();
  el->createTimer(timer, "Hello timer", timedOut);
  timer->setTimeout(10);
  timer->start();

  RGLIB_GET_MONOTONIC_TIME(start);
  el->run();
  RGLIB_GET_MONOTONIC_TIME(end);

  ASSERT("At least 10 ms elapsed", (end - start).toDouble() >= 0.01);
}

//------------------------------------------------------------------------------
void timerTimesOut()
{
  TimerTimedOut timedOut;
  std::unique_ptr<rglib::ITimer> timer;
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IEventLoop::create(el);

  el->init();
  el->createTimer(timer, "Hello timer", timedOut);
  timer->setTimeout(0);
  timer->start();

  el->run();

  ASSERT("Timer timed out", timedOut.hastimedOut());
}

//------------------------------------------------------------------------------
void canCreateTimer()
{
  TimerTimedOut timedOut;
  std::unique_ptr<rglib::ITimer> timer;
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IEventLoop::create(el);

  ASSERT_EQUALS(0, el->createTimer(timer, "Hello timer", timedOut));
  ASSERT("Timer is created", timer);
}

//------------------------------------------------------------------------------
void handleIoPersists()
{
  std::unique_ptr<rglib::IThread> thread;
  std::unique_ptr<rglib::IEventLoop> el;
  int comPipe[2];

  ASSERT_EQUALS(0, pipe(comPipe));

  IoHandler ioHandler(comPipe[0]);

  rglib::IThread::create(thread);
  rglib::IEventLoop::create(el);
  el->init();

  ASSERT_EQUALS(
      0,
      el->addHandledIo(
          ioHandler, rglib::IEventLoop::READ | rglib::IEventLoop::PERSIST)
      );

  LoopThread loopThread(*el);
  thread->start(loopThread);
  usleep(1000);
  write(comPipe[1], "hello", 5);
  usleep(1000);

  ASSERT_EQUALS(1, thread->isRunning());
  write(comPipe[1], "world", 5);
  usleep(1000);
  el->breakLoop();
  usleep(1000);

  ASSERT_EQUALS(true, ioHandler.hadData());
  ASSERT("Same String", std::string("helloworld") == ioHandler.getData());

  close(comPipe[0]);
  close(comPipe[1]);
}

//------------------------------------------------------------------------------
void handleIo()
{
  std::unique_ptr<rglib::IThread> thread;
  std::unique_ptr<rglib::IEventLoop> el;
  int comPipe[2];

  ASSERT_EQUALS(0, pipe(comPipe));

  IoHandler ioHandler(comPipe[0]);

  rglib::IThread::create(thread);
  rglib::IEventLoop::create(el);
  el->init();

  ASSERT_EQUALS(0, el->addHandledIo(ioHandler, rglib::IEventLoop::READ));

  LoopThread loopThread(*el);
  thread->start(loopThread);
  usleep(1000);
  write(comPipe[1], "hello", 5);
  usleep(1000);

  ASSERT_EQUALS(0, thread->isRunning());
  ASSERT_EQUALS(true, ioHandler.hadData());
  ASSERT("Same String", std::string("hello") == ioHandler.getData());

  close(comPipe[0]);
  close(comPipe[1]);
}

//------------------------------------------------------------------------------
void handleSignal()
{
  std::unique_ptr<rglib::IThread> thread;
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IThread::create(thread);
  rglib::IEventLoop::create(el);
  SignalHandler signalHandler(*el);

  el->init();
  ASSERT_EQUALS(0, el->addHandledSignal(signalHandler, SIGUSR1));

  LoopThread loopThread(*el);
  thread->start(loopThread);
  usleep(1000);
  kill(getpid(), SIGUSR1);
  usleep(1000);

  ASSERT_EQUALS(0, thread->isRunning());
  ASSERT_EQUALS(true, signalHandler.hadSignal());
  ASSERT_EQUALS(SIGUSR1, signalHandler.getSignal());
  ASSERT_EQUALS(0, loopThread.getElRetCode());
}

//------------------------------------------------------------------------------
void initEventLoop()
{
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IEventLoop::create(el);

  ASSERT_EQUALS(0, el->init());
}

//------------------------------------------------------------------------------
void createSucceeds()
{
  std::unique_ptr<rglib::IEventLoop> el;

  rglib::IEventLoop::create(el);

  ASSERT("event loop created", el.get() != nullptr);
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(createSucceeds);
  RUN(initEventLoop);
  RUN(handleSignal);
  RUN(handleIo);
  RUN(handleIoPersists);
  RUN(canCreateTimer);
  RUN(timerTimesOut);
  RUN(timerTimesOutWithRightDelay);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  return runTests();
}
