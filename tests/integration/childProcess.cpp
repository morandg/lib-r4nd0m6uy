/*
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <signal.h>
#include <unistd.h>

#include <rglib/process/IChildProcess.hpp>

#include "tinytest/tinytest.h"

/* Constants */
static const std::string SLEEP_CMD  = "/bin/sleep";
static const std::string ECHO_CMD   = "/bin/echo";
static const std::string CAT_CMD    = "/bin/cat";

/* Locals */
std::string sleepCmd;
std::string echoCmd;
std::string catCmd;

//------------------------------------------------------------------------------
void canWriteToStdin()
{
  std::string writtenText = "Hello world";
  int writeRet;
  unsigned int buffSize = 32;
  char buffer[buffSize];
  int readRet;
  std::unique_ptr<rglib::IChildProcess> process;

  rglib::IChildProcess::create(process);

  process->start(catCmd);
  writeRet = process->getStdIn().write(
      writtenText.c_str(), writtenText.size());
  ASSERT_EQUALS((int)writtenText.size(), writeRet);
  usleep(100000);

  readRet = process->getStdOut().read(buffer, buffSize);
  ASSERT("Read something", readRet > 0);
  ASSERT("Read right message", writtenText == std::string(buffer, readRet));
}

//------------------------------------------------------------------------------
void canReadStdout()
{
  unsigned int buffSize = 32;
  char buffer[buffSize];
  int readRet;
  std::unique_ptr<rglib::IChildProcess> process;
  std::string echoArgs = "Hello world";

  rglib::IChildProcess::create(process);

  process->start(echoCmd + " -n " + echoArgs);
  usleep(100000);
  readRet = process->getStdOut().read(buffer, buffSize);

  ASSERT("Read something", readRet > 0);
  ASSERT("Read right message", echoArgs == std::string(buffer, readRet));
}

//------------------------------------------------------------------------------
void canBeStoppped()
{
  std::unique_ptr<rglib::IChildProcess> process;

  rglib::IChildProcess::create(process);

  ASSERT_EQUALS(0, process->start(sleepCmd + " 10"));
  ASSERT("Process is running", process->isRunning());
  ASSERT_EQUALS(0, process->kill(SIGTERM));
  usleep(1000);
  ASSERT("Process is no more running", !process->isRunning());
}

//------------------------------------------------------------------------------
void canBeCreated()
{
  std::unique_ptr<rglib::IChildProcess> process;

  rglib::IChildProcess::create(process);

  ASSERT("Can be created", process.get() != nullptr);
  ASSERT("Process is not running", !process->isRunning());
}

//------------------------------------------------------------------------------
int runTests()
{
  RUN(canBeCreated);
  RUN(canBeStoppped);
  RUN(canReadStdout);
  RUN(canWriteToStdin);

  return TEST_REPORT();
}

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  // XXX: parse args?
  sleepCmd = SLEEP_CMD;
  echoCmd = ECHO_CMD;
  catCmd = CAT_CMD;

  return runTests();
}
