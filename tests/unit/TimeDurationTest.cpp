/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "rglib/time/TimeDuration.hpp"

using namespace rglib;

//------------------------------------------------------------------------------
TEST_GROUP(TimeDuration)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(TimeDuration, operatorTimeEqualInt)
{
  TimeDuration td = TimeDuration::ONE_MINUTE();

  td *= 3;

  CHECK_EQUAL(180, td.getSeconds());
  CHECK_EQUAL(0, td.getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, moreSophisticatedArithmetic)
{
  TimeDuration threeMinutes =
      TimeDuration::ONE_MINUTE() * 3 + TimeDuration::ONE_MILLISECOND() * 1500;

  CHECK_EQUAL(181, threeMinutes.getSeconds());
  CHECK_EQUAL(500000000, threeMinutes.getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, operatorTimeInt)
{
  TimeDuration threeMinutes = TimeDuration::ONE_MINUTE() * 3;

  CHECK_EQUAL(180, threeMinutes.getSeconds());
  CHECK_EQUAL(0, threeMinutes.getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ONE_MILLISECONDConstructor)
{
  CHECK_EQUAL(0, TimeDuration::ONE_MILLISECOND().getSeconds());
  CHECK_EQUAL(1000000, TimeDuration::ONE_MILLISECOND().getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ONE_HOURConstructor)
{
  CHECK_EQUAL(3600, TimeDuration::ONE_HOUR().getSeconds());
  CHECK_EQUAL(0, TimeDuration::ONE_HOUR().getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ONE_MINUTEConstructor)
{
  CHECK_EQUAL(60, TimeDuration::ONE_MINUTE().getSeconds());
  CHECK_EQUAL(0, TimeDuration::ONE_MINUTE().getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ONE_SECONDConstructor)
{
  CHECK_EQUAL(1, TimeDuration::ONE_SECOND().getSeconds());
  CHECK_EQUAL(0, TimeDuration::ONE_MINUTE().getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ZEROConstructor)
{
  CHECK_EQUAL(0, TimeDuration::ZERO().getSeconds());
  CHECK_EQUAL(0, TimeDuration::ZERO().getNseconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, toDoubleNegative)
{
  TimeDuration duration1(3, 123450000, false);

  DOUBLES_EQUAL(-3.12345, duration1.toDouble(), 0.00001);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, toDouble)
{
  TimeDuration duration1(3, 123450000);

  DOUBLES_EQUAL(3.12345, duration1.toDouble(), 0.00001);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, minEqualOperator)
{
  TimeDuration duration1(3, 7);
  TimeDuration duration2(1, 4);

  duration2 -= duration1;

  CHECK_EQUAL(2, duration2.getSeconds());
  CHECK_EQUAL(3, duration2.getNseconds());
  CHECK(!duration2.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, plusEqualOperator)
{
  TimeDuration duration1(1, 4);
  TimeDuration duration2(3, 7);

  duration2 += duration1;

  CHECK_EQUAL(4, duration2.getSeconds());
  CHECK_EQUAL(11, duration2.getNseconds());
  CHECK(duration2.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativeMinusNegativePositiveResult)
{
  TimeDuration duration1(1, 900000000, false);
  TimeDuration duration2(3, 0, false);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativeMinusNegativeNegativeResult)
{
  TimeDuration duration1(3, 0, false);
  TimeDuration duration2(1, 900000000, false);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativeMinusPositiveNegativeResult)
{
  TimeDuration duration1(3, 0, false);
  TimeDuration duration2(1, 900000000);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(4, duration3.getSeconds());
  CHECK_EQUAL(900000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativePlusPositivePositiveResult)
{
  TimeDuration duration1(1, 900000000, false);
  TimeDuration duration2(3, 0);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativePlusPositiveNegativeResult)
{
  TimeDuration duration1(3, 0, false);
  TimeDuration duration2(1, 900000000);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositivePlusNegativeNegativeResult)
{
  TimeDuration duration1(1, 900000000);
  TimeDuration duration2(3, 0, false);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositivePlusNegativePositiveResult)
{
  TimeDuration duration1(3, 0);
  TimeDuration duration2(1, 900000000, false);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositiveMinusPositiveResultNegative)
{
  TimeDuration duration1(1, 900000000);
  TimeDuration duration2(3, 0);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositiveMinusPositiveNsecUnderFlow)
{
  TimeDuration duration1(3, 0);
  TimeDuration duration2(1, 900000000);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(1, duration3.getSeconds());
  CHECK_EQUAL(100000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositiveMinusPositiveNoOverflow)
{
  TimeDuration duration1(3, 900000000);
  TimeDuration duration2(1, 900000000);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(2, duration3.getSeconds());
  CHECK_EQUAL(0, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositiveMinusNegative)
{
  TimeDuration duration1(3, 900000000, true);
  TimeDuration duration2(1, 900000000, false);
  TimeDuration duration3 = duration1 - duration2;

  CHECK_EQUAL(5, duration3.getSeconds());
  CHECK_EQUAL(800000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NegativePlusNegative)
{
  TimeDuration duration1(3, 900000000, false);
  TimeDuration duration2(1, 900000000, false);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(5, duration3.getSeconds());
  CHECK_EQUAL(800000000, duration3.getNseconds());
  CHECK(!duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, PositivePlusPositive)
{
  TimeDuration duration1(3, 900000000);
  TimeDuration duration2(1, 900000000);
  TimeDuration duration3 = duration1 + duration2;

  CHECK_EQUAL(5, duration3.getSeconds());
  CHECK_EQUAL(800000000, duration3.getNseconds());
  CHECK(duration3.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ToStringNegative)
{
  TimeDuration duration1(435, 532, false);
  std::string strDuration = duration1.toString();

  STRCMP_EQUAL("-435.000000532", strDuration.c_str());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ToStringPositive)
{
  TimeDuration duration1(2, 3);
  std::string strDuration = duration1.toString();

  STRCMP_EQUAL("2.000000003", strDuration.c_str());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, equalNegativeIsNotBiggerButBiggerOrEqual)
{
  uint64_t sec =  1234;
  uint64_t nsec =  4321;
  TimeDuration duration1(sec, nsec, false);

  CHECK(!(duration1 > duration1));
  CHECK(duration1 >= duration1);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, equalPositiveIsNotBiggerButSmallerOrEqual)
{
  uint64_t sec =  4356;
  uint64_t nsec =  654;
  TimeDuration duration1(sec, nsec);

  CHECK(!(duration1 > duration1));
  CHECK(duration1 >= duration1);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, TowNegativeSmallerIsNotBigger)
{
  TimeDuration duration1(3, 900000000, false);
  TimeDuration duration2(3, 0, false);

  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, TwoNegativeSecondEqualNanoBigger)
{
  TimeDuration duration1(2, 0, false);
  TimeDuration duration2(2, 1, false);

  CHECK(duration1 > duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, TwoNegativeBigger)
{
  TimeDuration duration1(1, 0, false);
  TimeDuration duration2(2, 0, false);

  CHECK(duration1 > duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, EqualButNotSameSignIsBigger)
{
  uint64_t sec =  1;
  uint64_t nsec =  0;
  TimeDuration duration1(sec, nsec, true);
  TimeDuration duration2(sec, nsec, false);

  CHECK(duration1 > duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, BiggerNanoSecond)
{
  uint64_t sec =  1;
  uint64_t nsec =  1;
  TimeDuration duration1(sec, nsec);
  TimeDuration duration2(sec, nsec - 1);

  CHECK(duration1 > duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, BiggerSecond)
{
  uint64_t sec =  1;
  uint64_t nsec =  0;
  TimeDuration duration1(sec, nsec);
  TimeDuration duration2(sec - 1, nsec);

  CHECK(duration1 > duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, equalNegativeIsNotSmallerButSmallerOrEqual)
{
  uint64_t sec =  2345;
  uint64_t nsec =  3456;
  TimeDuration duration1(sec, nsec, false);

  CHECK(!(duration1 < duration1));
  CHECK(duration1 <= duration1);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, equalPositiveIsNotSmallerButSmallerOrEqual)
{
  uint64_t sec =  5674;
  uint64_t nsec =  4234;
  TimeDuration duration1(sec, nsec);

  CHECK(!(duration1 < duration1));
  CHECK(duration1 <= duration1);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, BiggerIsNotSmaller)
{
  TimeDuration duration1(3, 0);
  TimeDuration duration2(1, 900000000);

  CHECK(!(duration1 < duration2));
}

//------------------------------------------------------------------------------
TEST(TimeDuration, TwoNegativeSecondEqualNanoSmaller)
{
  TimeDuration duration1(2, 1, false);
  TimeDuration duration2(2, 0, false);

  // -2.0001 < --2.0000
  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, TwoNegativeSmaller)
{
  TimeDuration duration1(2, 0, false);
  TimeDuration duration2(1, 0, false);

  // -2 < -1
  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, EqualButNotSameSignIsSmaller)
{
  uint64_t sec =  1;
  uint64_t nsec =  0;
  TimeDuration duration1(sec, nsec, false);
  TimeDuration duration2(sec, nsec, true);

  // -1 < 1
  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, SmallerNanoSecond)
{
  uint64_t sec =  1;
  uint64_t nsec =  1;
  TimeDuration duration1(sec, nsec - 1);
  TimeDuration duration2(sec, nsec);

  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, SmallerSecond)
{
  TimeDuration duration1(1, 900000000);
  TimeDuration duration2(3, 0);

  CHECK(duration1 < duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, SameNanoSecondAndSecondDifferentSignNotEqual)
{
  uint64_t sec =  2345;
  uint64_t nsec =  3456;
  TimeDuration duration1(sec, nsec, true);
  TimeDuration duration2(sec, nsec, false);

  CHECK(!(duration1 == duration2));
  CHECK(duration1 != duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, SameNanoSecondAndSecondEqual)
{
  uint64_t sec =  3456;
  uint64_t nsec =  6543;
  TimeDuration duration1(sec, nsec);
  TimeDuration duration2(sec, nsec);

  CHECK(duration1 == duration2);
  CHECK(!(duration1 != duration2));
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NotSameNanoSecondNotEqual)
{
  uint64_t sec =  1;
  uint64_t nsec =  1;
  TimeDuration duration1(sec, nsec);
  TimeDuration duration2(sec, nsec + 1);

  CHECK(!(duration1 == duration2));
  CHECK(duration1 != duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, NotSameSecondsNotEqual)
{
  uint64_t sec =  1;
  uint64_t nsec =  1;
  TimeDuration duration1(sec, nsec);
  TimeDuration duration2(sec + 1, nsec);

  CHECK(!(duration1 == duration2));
  CHECK(duration1 != duration2);
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ConstructorWithNegativeValue)
{
  TimeDuration duration(10, 5000000003, false);

  CHECK_EQUAL(15, duration.getSeconds());
  CHECK_EQUAL(3, duration.getNseconds());
  CHECK(!duration.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ConstructorWithNanoArgsBiggerThanSecond)
{
  TimeDuration duration(10, 5000000003);

  CHECK_EQUAL(15, duration.getSeconds());
  CHECK_EQUAL(3, duration.getNseconds());
  CHECK(duration.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ConstructorWithNanoSecondEqualSecond)
{
  TimeDuration duration(10, 1000000000);

  CHECK_EQUAL(11, duration.getSeconds());
  CHECK_EQUAL(0, duration.getNseconds());
  CHECK(duration.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ConstructorWithNSecOveflow)
{
  uint64_t sec =  1234;
  uint64_t nsec =  (TimeDuration::NANO_PER_SECOND * 2) + 3;
  TimeDuration duration(sec, nsec);

  CHECK_EQUAL(3, duration.getNseconds());
  CHECK_EQUAL(sec + 2, duration.getSeconds());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, ConstructorWithArgsNsecBelow1Second)
{
  uint64_t sec =  1234;
  uint64_t nsec =  324;
  TimeDuration duration(sec, nsec);

  CHECK_EQUAL(sec, duration.getSeconds());
  CHECK_EQUAL(nsec, duration.getNseconds());
  CHECK(duration.isPositive());
}

//------------------------------------------------------------------------------
TEST(TimeDuration, DefaultConstructor)
{
  TimeDuration duration;

  CHECK_EQUAL(0, duration.getSeconds());
  CHECK_EQUAL(0, duration.getNseconds());
  CHECK(duration.isPositive());
}

