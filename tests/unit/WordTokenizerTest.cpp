/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "rglib/lexical/TokenizerString.hpp"
#include "rglib/lexical/WordTokenizer.hpp"

using namespace rglib;

//------------------------------------------------------------------------------
TEST_GROUP(WordTokenizer)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(WordTokenizer, splitBufferStatic)
{
  std::string buffer = "abc def ghi";
  std::list<std::string> splitted;

  WordTokenizer::split(buffer, ' ', splitted);

  auto foundWord = splitted.begin();
  STRCMP_EQUAL("abc", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("def", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("ghi", (*foundWord).c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, splitBufferGivenListIsEmptied)
{
  std::string buffer = "abc  def     ghi";
  std::list<std::string> splitted;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  splitted.push_back("poison");
  wordTokenizer.split(' ', splitted);

  auto foundWord = splitted.begin();
  STRCMP_EQUAL("abc", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("def", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("ghi", (*foundWord).c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, splitBufferWithMoreThanOneSeparator)
{
  std::string buffer = "abc  def     ghi";
  std::list<std::string> splitted;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  wordTokenizer.split(' ', splitted);

  auto foundWord = splitted.begin();
  STRCMP_EQUAL("abc", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("def", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("ghi", (*foundWord).c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, splitBuffer)
{
  std::string buffer = "abc def ghi";
  std::list<std::string> splitted;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  wordTokenizer.split(' ', splitted);

  auto foundWord = splitted.begin();
  STRCMP_EQUAL("abc", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("def", (*foundWord).c_str());
  ++foundWord;
  STRCMP_EQUAL("ghi", (*foundWord).c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, twoWordsFoundSeparatorNotIncluded)
{
  std::string buffer = "abc def";
  std::string word;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  CHECK(wordTokenizer.getNextWord(' ', word));
  STRCMP_EQUAL("abc", word.c_str());
  CHECK_FALSE(wordTokenizer.getNextWord(' ', word));
  STRCMP_EQUAL("def", word.c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, getNextWordFound)
{
  std::string buffer = "abc ";
  std::string word;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  CHECK(wordTokenizer.getNextWord(' ', word));
  STRCMP_EQUAL("abc", word.c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, getNextWordNotFoundButWordBuiltToEndOfBuffer)
{
  std::string buffer = "abc";
  std::string word;
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);

  tokenizer.setBuffer(buffer);
  CHECK_FALSE(wordTokenizer.getNextWord(' ', word));
  STRCMP_EQUAL("abc", word.c_str());
}

//------------------------------------------------------------------------------
TEST(WordTokenizer, constructor)
{
  TokenizerString tokenizer;
  WordTokenizer wordTokenizer(tokenizer);
}
