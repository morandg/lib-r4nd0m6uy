/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "rglib/geometry/Vector2D.hpp"

using namespace rglib;

//------------------------------------------------------------------------------
TEST_GROUP(Vector2D)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Vector2D, constructorWithXY)
{
  int x = 11;
  int y = 22;
  Vector2D vector(x, y);

  CHECK_EQUAL(x, vector.getX());
  CHECK_EQUAL(y, vector.getY());
}

//------------------------------------------------------------------------------
TEST(Vector2D, defaultConstructor)
{
  Vector2D vector;

  CHECK_EQUAL(0, vector.getX());
  CHECK_EQUAL(0, vector.getY());
}
