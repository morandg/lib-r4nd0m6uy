/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "rglib/lexical/TokenizerString.hpp"

using namespace rglib;

//------------------------------------------------------------------------------
TEST_GROUP(TokenizerString)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(TokenizerString, setNewBufferResetRestartFromFirstToken)
{
  std::string buffer1 = "abc";
  std::string buffer2 = "def";
  char token;
  TokenizerString tokenizer;

  tokenizer.setBuffer(buffer1);
  tokenizer.nextToken(token);
  tokenizer.setBuffer(buffer2);
  tokenizer.nextToken(token);

  CHECK_EQUAL('d', token);
}

//------------------------------------------------------------------------------
TEST(TokenizerString, nextTokenNoMoreToken)
{
  char token;
  TokenizerString tokenizer;

  CHECK_FALSE(tokenizer.nextToken(token));
}

//------------------------------------------------------------------------------
TEST(TokenizerString, getTokenHasNoMoreToken)
{
  std::string buffer = "a";
  char token;
  TokenizerString tokenizer;

  tokenizer.setBuffer(buffer);

  tokenizer.nextToken(token);
  CHECK_FALSE(tokenizer.hasToken());
}

//------------------------------------------------------------------------------
TEST(TokenizerString, getToken)
{
  std::string buffer = "a";
  char token;
  TokenizerString tokenizer;

  tokenizer.setBuffer(buffer);

  CHECK(tokenizer.nextToken(token));
  CHECK_EQUAL('a', token);
}

//------------------------------------------------------------------------------
TEST(TokenizerString, hasTokenWithABuffer)
{
  std::string buffer = "a";
  TokenizerString tokenizer;

  tokenizer.setBuffer(buffer);

  CHECK(tokenizer.hasToken());
}

//------------------------------------------------------------------------------
TEST(TokenizerString, hasNotTokenWhenEmptyBuffer)
{
  TokenizerString tokenizer;

  CHECK_FALSE(tokenizer.hasToken());
}

//------------------------------------------------------------------------------
TEST(TokenizerString, constructor)
{
  TokenizerString tokenizer;
}
