/**
 * R4nd0m6uy's library
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTest/TestHarness.h>

#include "rglib/geometry/Rectangle.hpp"

using namespace rglib;

//------------------------------------------------------------------------------
TEST_GROUP(Rectangle)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Rectangle, isNotInWhenOnRight)
{
  Vector2D point(15, 5);
  Rectangle rec(0, 10, 10, 10);

  CHECK_FALSE(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isInWhenOnRightBorder)
{
  Vector2D point(10, 5);
  Rectangle rec(0, 10, 10, 10);

  CHECK(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isInWhenOnLeftBorder)
{
  Vector2D point(0, 5);
  Rectangle rec(0, 10, 10, 10);

  CHECK(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isNotInWhenOnLeft)
{
  Vector2D point(-1, 5);
  Rectangle rec(0, 10, 10, 10);

  CHECK_FALSE(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isNotInWhenOnBottom)
{
  Vector2D point(5, -1);
  Rectangle rec(0, 10, 10, 10);

  CHECK_FALSE(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isInWhenOnBottomBorder)
{
  Vector2D point(5, 0);
  Rectangle rec(0, 10, 10, 10);

  CHECK(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isInWhenOnTopBorder)
{
  Vector2D point(5, 10);
  Rectangle rec(0, 10, 10, 10);

  CHECK(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, isNotInWhenUpper)
{
  Vector2D point(5, 15);
  Rectangle rec(0, 10, 10, 10);

  CHECK_FALSE(rec.isIn(point));
}

//------------------------------------------------------------------------------
TEST(Rectangle, constructorWithXYWidthHeignt)
{
  int x = 111;
  int y = 222;
  int width = 333;
  int height = 444;
  Rectangle rec(x, y, width, height);

  CHECK_EQUAL(x, rec.getX());
  CHECK_EQUAL(y, rec.getY());
  CHECK_EQUAL(width, rec.getWidth());
  CHECK_EQUAL(height, rec.getHeight());
}

//------------------------------------------------------------------------------
TEST(Rectangle, defaultConstructor)
{
  Rectangle rec;

  CHECK_EQUAL(0, rec.getX());
  CHECK_EQUAL(0, rec.getY());
  CHECK_EQUAL(0, rec.getWidth());
  CHECK_EQUAL(0, rec.getHeight());
}
