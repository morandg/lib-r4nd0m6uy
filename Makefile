PROJECT             = r4nd0m6uy
BUILD_DIR           ?= build
LIB_NAME            = lib$(PROJECT)

# Version information
VERSION_MAJOR       = 0
VERSION_MINOR       = 0
VERSION_BUGFIX      = 1
VERSION_GIT         := $(shell git describe --tag --always --abbrev=5 --dirty)

# Lib sources and artifacts
LIB_STATIC_NAME     = $(LIB_NAME).a
LIB_STATIC          = $(BUILD_DIR)/$(LIB_STATIC_NAME)
LIB_SO_BASENAME     = lib$(PROJECT).so
LIB_SONAME          = $(LIB_SO_BASENAME).$(VERSION_MAJOR)
LIB_SHARED_NAME     = $(LIB_SONAME).$(VERSION_MINOR).$(VERSION_BUGFIX)
LIB_SHARED          = $(BUILD_DIR)/$(LIB_SONAME).$(VERSION_MINOR).$(VERSION_BUGFIX)
LIB_SOURCES         = lib/src/logging/ILogSink.cpp \
                      lib/src/logging/LogStream.cpp \
                      lib/src/logging/ILogger.cpp \
                      lib/src/logging/NullLogSink.cpp \
                      lib/src/logging/Logger.cpp \
                      lib/src/logging/null/LoggerNull.cpp \
                      lib/src/logging/console/LoggerConsole.cpp \
                      lib/src/logging/LoggerInstance.cpp \
                      lib/src/io/IIoStream.cpp \
                      lib/src/io/IFile.cpp \
                      lib/src/threads/IThreaded.cpp \
                      lib/src/threads/IThread.cpp \
                      lib/src/threads/IMutex.cpp \
                      lib/src/threads/ISemaphore.cpp \
                      lib/src/event-loop/ITimer.cpp \
                      lib/src/event-loop/ITimedOut.cpp \
                      lib/src/event-loop/IHandledSignal.cpp \
                      lib/src/event-loop/IHandledIo.cpp \
                      lib/src/event-loop/IEventLoop.cpp \
                      lib/src/ipc/ISocketClient.cpp \
                      lib/src/ipc/ISocketServer.cpp \
                      lib/src/ipc/IPipe.cpp \
                      lib/src/lexical/ITokenizer.cpp \
                      lib/src/lexical/TokenizerString.cpp \
                      lib/src/lexical/WordTokenizer.cpp \
                      lib/src/geometry/Vector2D.cpp \
                      lib/src/geometry/Rectangle.cpp \
                      lib/src/process/IChildProcess.cpp \
                      lib/src/process/IDaemonizer.cpp \
                      lib/src/time/null/CalendarNull.cpp \
                      lib/src/time/TimeDuration.cpp \
                      lib/src/time/ICalendar.cpp \
                      lib/src/time/CalendarInstance.cpp
PUBLIC_HEADERS      = $(shell find lib/include -name '*.hpp') \
                      lib/include/rglib/Version.hpp

# Mock lib sources and artifacts
LIB_MOCKS_NAME      = $(LIB_NAME)-mocks.a
LIB_MOCKS_STATIC    = $(BUILD_DIR)/$(LIB_MOCKS_NAME)
LIB_MOCKS_SOURCES   = tests/rglib-mocks/comparators/StdStringComparator.cpp \
                      tests/rglib-mocks/copiers/TimerUniquePtrCopier.cpp \
                      tests/rglib-mocks/mocks/event-loop/EventLoopMock.cpp \
                      tests/rglib-mocks/mocks/event-loop/TimerMock.cpp \
                      tests/rglib-mocks/mocks/io/FileMock.cpp
LIB_MOCKS_OBJS      = $(patsubst %.cpp,$(BUILD_DIR)/mocks/%.o,$(LIB_MOCKS_SOURCES))

# Test sources and artifacts
TEST_BIN            = $(BUILD_DIR)/lib$(PROJECT)_tests
TEST_SOURCES        := $(LIB_SOURCES) \
                       tests/unit/TokenizerStringTest.cpp \
                       tests/unit/WordTokenizerTest.cpp \
                       tests/unit/Vector2DTest.cpp \
                       tests/unit/RectangleTest.cpp \
                       tests/unit/TimeDurationTest.cpp \
                       tests/unit/main.cpp
TEST_OBJS           = $(patsubst %.cpp,$(BUILD_DIR)/mocks/%.o,$(TEST_SOURCES))

# POSIX support
# FIXME: Detect target OS
IS_POSIX            ?= 1
ifeq ($(IS_POSIX),1)
  LIB_SOURCES       += lib/src/io/posix/IoStreamPosix.cpp \
                       lib/src/io/posix/FilePosix.cpp \
                       lib/src/threads/posix/ThreadPosix.cpp \
                       lib/src/threads/posix/MutexPosix.cpp \
                       lib/src/threads/posix/SemaphorePosix.cpp \
                       lib/src/ipc/posix/SocketClientPosix.cpp \
                       lib/src/ipc/posix/SocketServerPosix.cpp \
                       lib/src/ipc/posix/PipePosix.cpp \
                       lib/src/process/posix/ChildProcessPosix.cpp \
                       lib/src/process/posix/DaemonizerPosix.cpp \
                       lib/src/time/posix/CalendarPosix.cpp
  LIB_EXTRA_CFLAGS  += -D_IS_POSIX_
  PKGCONFIG_EXTRA_FLAGS += -lpthread
endif

# Add libevent support when found
HAS_LIBEVENT        = $(shell pkg-config libevent && echo 1)
ifeq ($(HAS_LIBEVENT),1)
  LIB_SOURCES        += lib/src/event-loop/libevent/HandledByLibevent.cpp \
                        lib/src/event-loop/libevent/EventBaseLibevent.cpp \
                        lib/src/event-loop/libevent/HandledSignalLibevent.cpp \
                        lib/src/event-loop/libevent/HandledIoLibevent.cpp \
                        lib/src/event-loop/libevent/TimerLibevent.cpp \
                        lib/src/event-loop/libevent/EventLoopLibevent.cpp
  LIB_EXTRA_CFLAGS   += -D_HAS_LIBEVENT_
  PKG_CONFIG_PRIVATE_LIBS += libevent

  COMMON_CFLAGS      += $(shell pkg-config --cflags libevent)
  LIBEVENT_LDFLAGS   = $(shell pkg-config --libs libevent)
endif

# Add log4cplus support when found
HAS_LOG4CPLUS       = $(shell pkg-config log4cplus && echo 1)
ifeq ($(HAS_LOG4CPLUS),1)
  LIB_SOURCES        += lib/src/logging/log4cplus/LoggerLog4CPlus.cpp
  LIB_EXTRA_CFLAGS   += -D_HAS_LOG4CPLUS_
  PKG_CONFIG_PRIVATE_LIBS += log4cplus

  COMMON_CFLAGS      += $(shell pkg-config --cflags log4cplus)
  LOG4CPLUS_LDFLAGS  = $(shell pkg-config --libs log4cplus)
endif

LIB_OBJS_STATIC     = $(patsubst %.cpp,$(BUILD_DIR)/static/%.o,$(LIB_SOURCES))
LIB_OBJS_SHARED     = $(patsubst %.cpp,$(BUILD_DIR)/shared/%.o,$(LIB_SOURCES))

# CppUTest library
HAS_CPPUTEST        = $(shell pkg-config cpputest && echo 1)
ifeq ($(HAS_CPPUTEST),1)
  CPPUTEST_FLAGS    := $(shell pkg-config --cflags cpputest)
  CPPUTEST_LDFLAGS  := $(shell pkg-config --libs cpputest)
endif

# Integration tests
TEST_FILE           = $(BUILD_DIR)/tests/file
TEST_SEMAPHORE      = $(BUILD_DIR)/tests/semaphore
TEST_THREAD         = $(BUILD_DIR)/tests/thread
TEST_LOOP_LIBEVENT  = $(BUILD_DIR)/tests/eventLoop
TEST_LOG            = $(BUILD_DIR)/tests/logger
TEST_PIPE           = $(BUILD_DIR)/tests/pipe
TEST_CHILDPROC      = $(BUILD_DIR)/tests/childProcess
TEST_MUTEX          = $(BUILD_DIR)/tests/mutex
TEST_CALENDAR       = $(BUILD_DIR)/tests/calendar

# Generated depedecy files
DEPS                = $(LIB_OBJS_STATIC:.o=.d) \
                      $(LIB_OBJS_SHARED:.o=.d) \
                      $(TEST_OBJS:.o=.d) \
                      $(LOB_MOCKS_OBJS:.o=.d)

# Compiler options
COMMON_CFLAGS       += -Wall -Werror -Wextra -MMD -Ilib/include

# Debug/Release mode
ifneq ($(DEBUG),)
  COMMON_CFLAGS     += -g
  BUILD_DIR         := $(BUILD_DIR)/debug
else
  COMMON_CFLAGS     += -DNDEBUG -O3
  BUILD_DIR         := $(BUILD_DIR)/release
endif

CFLAGS              += $(COMMON_CFLAGS)
CXXFLAGS            += $(COMMON_CFLAGS) -std=c++14

# Silence make
ifneq ($(V),)
  SILENCE           =
else
  SILENCE           = @
endif

# install
DESTDIR                 ?=
PREFIX                  ?= /usr/local
INSTALL_DIR             = $(DESTDIR)$(PREFIX)
INCLUDE_DIR             = $(INSTALL_DIR)/include
LIB_DIR                 = $(INSTALL_DIR)/lib
INSTALLED_HEADERS       = $(patsubst %.hpp,$(INCLUDE_DIR)/%.hpp,$(subst lib/include/,,$(PUBLIC_HEADERS)))
PKG_CONFIG_DEST         = $(INSTALL_DIR)/lib/pkgconfig/rglib.pc
PKG_CONFIG_MOCKS_DEST   = $(INSTALL_DIR)/lib/pkgconfig/rglib-mocks.pc
MOCKS_HEADERS           = $(shell find tests/rglib-mocks -name '*.hpp')
INSTALLED_MOCK_HEADERS  = $(patsubst %.hpp,$(INCLUDE_DIR)/%.hpp,$(subst tests/,,$(MOCKS_HEADERS)))

# Fancy output
SHOW_COMMAND        := @printf "%-15s%s\n"
SHOW_CXX            := $(SHOW_COMMAND) "[ $(CXX) ]"
SHOW_CLEAN          := $(SHOW_COMMAND) "[ CLEAN ]"
SHOW_GEN            := $(SHOW_COMMAND) "[ GEN ]"
SHOW_AR             := $(SHOW_COMMAND) "[ $(AR) ]"
SHOW_MKDIR          := $(SHOW_COMMAND) "[ mkdir ]"
SHOW_INSTALL        := $(SHOW_COMMAND) "[ install ]"

################################################################################
# Default target
################################################################################
DEFAULT_TARGET =  $(LIB_STATIC) $(LIB_SHARED)

ifeq ($(HAS_CPPUTEST),1)
  DEFAULT_TARGET += run_unit_tests
endif

all: $(DEFAULT_TARGET)
.PHONY: all

static: $(LIB_STATIC)
.PHONY: static

libmocks: $(LIB_MOCKS_STATIC)
.PHONY: mockslib

shared: $(LIB_SHARED)
.PHONY: shared

# Take care of compiler generated depedencies
-include $(DEPS)

################################################################################
# Help message
################################################################################
help:
	@echo "all               Build static and shared library"
	@echo "static            Build static library"
	@echo "shared            Build shared library"
	@echo "tests             Build the tests"
	@echo "mockslib          Build mocking library"
	@echo "run_tests         Build and run the tests"
	@echo "run_unit_tests    Build and run the unit tests"
	@echo "install           Install the shared and static library"
	@echo "install_static    Install the static library"
	@echo "install_shared    Install the shared library"
	@echo "install_mocks     Install the mocking library"
	@echo "clean             Clean everything"
	@echo ""
	@echo "Variables:"
	@echo "DEBUG             Set to any value to build with debug symbols"
	@echo "V                 Set to any value for verbose build"
	@echo "PREFIX            Install prefix"
	@echo "DESTDIR           Install destination"
.PHONY: help

################################################################################
# Version generated file
################################################################################
lib/include/rglib/Version.hpp: lib/include/rglib/Version.hpp.in Makefile $(LIB_SOURCES)
	$(SHOW_GEN) $@
	$(SILENCE)sed \
	  -e 's/%%VERSION_MAJOR%%/$(VERSION_MAJOR)/g' \
	  -e 's/%%VERSION_MINOR%%/$(VERSION_MINOR)/g' \
	  -e 's/%%VERSION_BUGFIX%%/$(VERSION_BUGFIX)/g' \
	  -e 's/%%VERSION_GIT%%/$(VERSION_GIT)/g' \
	  $< > $@

################################################################################
# Static library
################################################################################
$(LIB_STATIC): $(LIB_OBJS_STATIC)
	$(SHOW_AR) $@
	$(SILENCE)$(AR) rcs $@ $^

$(BUILD_DIR)/static/%.o: %.cpp
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(LIB_EXTRA_CFLAGS) -c $< -o $@

################################################################################
# Shared library
################################################################################
$(LIB_SHARED): $(LIB_OBJS_SHARED)
	$(SHOW_CXX) $@
	$(SILENCE)$(CXX) -o $@ $(LDFLAGS) -shared -Wl,-soname,$(LIB_SONAME) $^

$(BUILD_DIR)/shared/%.o: %.cpp
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(LIB_EXTRA_CFLAGS) -fPIC -c $< -o $@

################################################################################
# Mock library
################################################################################
$(LIB_MOCKS_STATIC): $(LIB_MOCKS_OBJS)
	$(SHOW_AR) $@
	$(SILENCE)$(AR) rcs $@ $^

$(BUILD_DIR)/mocks/%.o: %.cpp
ifneq ($(HAS_CPPUTEST),1)
	$(error CppUTest not found, cannot build mocking library)
endif
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(CPPUTEST_FLAGS) -c $< -o $@

################################################################################
# Unit tests
################################################################################
unit_tests: $(TEST_BIN)
.PHONY: unit_tests

run_unit_tests: $(BUILD_DIR)/.tests_passed
.PHONY: run_unit_tests

$(BUILD_DIR)/.tests_passed: $(TEST_BIN)
	$(SILENCE)./$< || rm $<
	$(SILENCE)touch $@

$(TEST_BIN): $(TEST_OBJS)
	$(SHOW_CXX) $@
	$(SILENCE)$(CXX) $(TEST_OBJS) $(CPPUTEST_LDFLAGS) -o $@

$(BUILD_DIR)/tests/unit/%.o: %.cpp
ifneq ($(HAS_CPPUTEST),1)
	$(error CppUTest not found, cannot build the tests)
endif
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(CPPUTEST_FLAGS) -c $< -o $@

################################################################################
# install library
################################################################################
install: install_static install_shared
.PHONY: install

install_static: $(LIB_DIR)/$(LIB_STATIC_NAME) $(INSTALLED_HEADERS) $(PKG_CONFIG_DEST)
.PHONY: install_static

install_shared: $(LIB_DIR)/$(LIB_SHARED_NAME) $(INSTALLED_HEADERS) $(PKG_CONFIG_DEST)
.PHONY: install_static

$(INSTALL_DIR)/%.hpp: lib/%.hpp
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 $< $@

$(LIB_DIR)/$(LIB_STATIC_NAME): $(LIB_STATIC)
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 $< $@

$(LIB_DIR)/$(LIB_SHARED_NAME): $(LIB_SHARED)
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0755 $< $@
	$(SILENCE)ln -sf $(LIB_SHARED_NAME) $(LIB_DIR)/$(LIB_SO_BASENAME)
	$(SILENCE)ln -sf $(LIB_SHARED_NAME) $(LIB_DIR)/$(LIB_SONAME)
	$(SILENCE)ln -sf $(LIB_SHARED_NAME) $(LIB_DIR)/$(LIB_SONAME).$(VERSION_BUGFIX)

gen_pkg_config:
	$(SHOW_GEN) rglib.pc
	$(SILENCE)sed \
	  -e 's,%%PREFIX%%,$(PREFIX),g' \
	  -e 's/%%VERSION_MAJOR%%/$(VERSION_MAJOR)/g' \
	  -e 's/%%VERSION_MINOR%%/$(VERSION_MINOR)/g' \
	  -e 's/%%VERSION_BUGFIX%%/$(VERSION_BUGFIX)/g' \
	  -e 's/%%VERSION_GIT%%/$(VERSION_GIT)/g' \
	  -e 's/%%PKG_CONFIG_PRIVATE_LIBS%%/$(PKG_CONFIG_PRIVATE_LIBS)/g' \
	  -e 's/%%PKGCONFIG_EXTRA_FLAGS%%/$(PKGCONFIG_EXTRA_FLAGS)/g' \
	  pkg-config/rglib.pc.in > pkg-config/rglib.pc
.PHONY: gen_pkg_config

$(PKG_CONFIG_DEST): gen_pkg_config
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 pkg-config/rglib.pc $@

################################################################################
# install mocking library
################################################################################
install_mocks: $(LIB_DIR)/$(LIB_MOCKS_NAME) $(PKG_CONFIG_MOCKS_DEST) $(INSTALLED_MOCK_HEADERS)
.PHONY:install_mocks

$(INSTALL_DIR)/include/%.hpp: tests/%.hpp
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 $< $@

gen_mocks_pkg_config:
	$(SHOW_GEN) rglib-mocks.pc
	$(SILENCE)sed \
	  -e 's,%%PREFIX%%,$(PREFIX),g' \
	  -e 's/%%VERSION_MAJOR%%/$(VERSION_MAJOR)/g' \
	  -e 's/%%VERSION_MINOR%%/$(VERSION_MINOR)/g' \
	  -e 's/%%VERSION_BUGFIX%%/$(VERSION_BUGFIX)/g' \
	  -e 's/%%VERSION_GIT%%/$(VERSION_GIT)/g' \
	  pkg-config/rglib-mocks.pc.in > pkg-config/rglib-mocks.pc
.PHONY: gen_mocks_pkg_config

$(PKG_CONFIG_MOCKS_DEST): gen_mocks_pkg_config
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 pkg-config/rglib-mocks.pc $@

$(LIB_DIR)/$(LIB_MOCKS_NAME): $(LIB_MOCKS_STATIC)
	$(SHOW_INSTALL) $@
	$(SILENCE)install -D -m 0644 $< $@

################################################################################
# Integration tests
################################################################################
$(TEST_FILE): tests/integration/file.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^

$(TEST_SEMAPHORE): tests/integration/semaphore.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^ -lpthread

$(TEST_THREAD): tests/integration/thread.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^ -lpthread

$(TEST_LOOP_LIBEVENT): tests/integration/eventLoop.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^ -lpthread $(LIBEVENT_LDFLAGS)

$(TEST_LOG): tests/integration/logger.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $(LOG4CPLUS_LDFLAGS) $^

$(TEST_PIPE): tests/integration/pipe.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^

$(TEST_CHILDPROC): tests/integration/childProcess.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^

$(TEST_MUTEX): tests/integration/mutex.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^ -lpthread

$(TEST_CALENDAR): tests/integration/calendar.cpp $(LIB_STATIC)
	$(SHOW_CXX) $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) -o $@ $(CXXFLAGS) $^ -lpthread

ALL_TESTS = \
  $(TEST_FILE) \
  $(TEST_SEMAPHORE) \
  $(TEST_THREAD) \
  $(TEST_LOG) \
  $(TEST_PIPE) \
  $(TEST_CHILDPROC) \
  $(TEST_MUTEX) \
  $(TEST_CALENDAR)

ifeq ($(HAS_LIBEVENT),1)
  ALL_TESTS += $(TEST_LOOP_LIBEVENT)
endif

tests: $(ALL_TESTS)
.PHONY: tests

run_tests: tests
	$(SILENCE)./$(TEST_FILE) || rm $(TEST_FILE)
	$(SILENCE)./$(TEST_SEMAPHORE) || rm $(TEST_SEMAPHORE)
	$(SILENCE)./$(TEST_THREAD) || rm $(TEST_THREAD)
	$(SILENCE)./$(TEST_LOG) || rm $(TEST_LOG)
	$(SILENCE)./$(TEST_PIPE) || rm $(TEST_PIPE)
	$(SILENCE)./$(TEST_CHILDPROC) || rm $(TEST_CHILDPROC)
	$(SILENCE)./$(TEST_MUTEX) || rm $(TEST_MUTEX)
	$(SILENCE)./$(TEST_CALENDAR) || rm $(TEST_CALENDAR)
ifeq ($(HAS_LIBEVENT),1)
	$(SILENCE)./$(TEST_LOOP_LIBEVENT) || rm $(TEST_LOOP_LIBEVENT)
endif
.PHONY: run_tests

################################################################################
# Cleanup
################################################################################
clean:
	$(SHOW_CLEAN) $(BUILD_DIR)
	$(SILENCE)rm -rf $(BUILD_DIR) lib/include/rglib/Version.hpp pkg-config/rglib.pc

.PHONY: cleann
